<?php

return array (
    'desc_long_local5'     => '<ul><li><a href="/carparts">local automotive</a></li>'.
                                '<li><a href="/furniture">Furniture</a></li>'.
                                '<li><a href="/joblot">Job lot bargains</a></li>'.
                                '<li><a href="/baby">Baby bargains</a></li>'.
                                '<li><a href="/laptop">Local Laptops</a></li></ul>',

    'filter_pc'         => 'Enter postal code',

    'filter_pc_max'     => 'You must enter a full Australian postcode.\n'.
                            'It must only contain up to 6 numbers',
    # ".$lang->getString('')."
);
