<?php

return array (

    'desc_long_local5'     => '<ul class=\'locallist\'>'.
                                '<li><a href="/carparts">local automotive</a></li>'.
                                '<li><a href="/furniture">furniture</a></li>'.
                                '<li><a href="/joblot">job lot bargains</a></li>'.
                                '<li><a href="/baby">Baby bargains</a></li>'.
                                '<li><a href="/laptop">local laptops</a></li></ul>',

    'filter_pc_max'     => 'You must enter a full Canada postcode.\n'.
                            'It must only contain up to ten letters and numbers',
    # ".$lang->getString('')."
);
