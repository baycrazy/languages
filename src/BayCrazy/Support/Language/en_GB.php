<?php

return array (

    #imagetags

    'tag_bc_logo'           => 'bayCrazyLogo.png',
    'tag_search_button_img' => 'searchbutton.png',

    'header_title'          => 'Find hidden bargains on eBay',

    #searchstrings

    'arg_local'             => '("cash on collection","collection only", '.
                                '"buyer must collect","pick-up only","buyer collects", '.
                                '"buyer to collect","collect in person",'.
                                '"collection in person","collect only","pickup only")',

    'arg_unwanted'          => '("unwanted gift", "unwanted present", "unwanted birthday", '.
                               '"unwanted Christmas", "unwanted XMas", "unwanted upgrade", '.
                               '"unwanted upgarde")',

    'arg_vintage'           => 'vintage',

    #meta keywords

    'kwd_local'             => 'auction bargains, ebay local bargains, ebay search, '.
                               'ebay local bargain finder, ebay misspelt, '.
                               'local bargain search, misspelling search, '.
                               'auction misseplling search',

    'kwd_cars'             => 'auction bargains, ebay local car bargains, ebay car search, '.
                               'ebay local car finder, ebay misspelt, '.
                               'local bargain search, misspelling search, '.
                               'auction misseplling search',

    'kwd_unwanted'          => 'unwanted gift, unwanted present, BNIB, unwanted Upgrade, '.
                               'auction bargains, ebay local bargains, ebay search, '.
                               'ebay local bargain finder, ebay misspelt, '.
                               'local bargain search, misspelling search, '.
                               'auction misseplling search',

    #nav
    'nav_home'              => 'Home',
    'nav_local'             => 'Local',
    'nav_misspelled'        => 'Misspelt',
    'nav_nightowl'          => 'Night',
    'nav_rightnow'          => 'Ending',
    'nav_manchester'        => 'Manchester',
    'nav_unwanted'          => 'Unwanted',
    'nav_forum'             => 'Join Us on Facebook',
    'nav_furniture'         => 'Furniture',
    'nav_joblot'            => 'Job-Lots',
    'nav_baby'              => 'Baby Items',
    'nav_carparts'          => 'Car Parts',
    'nav_cars'              => 'Cars',
    'nav_laptop'            => 'Laptops',
    'nav_contact_us'        => 'Contact Us',
    'nav_help'              => 'Help',
    'nav_faq'               => 'Help',
    'nav_amazon'            => 'Search Amazon',
    'nav_buynow'            => 'Buy It Now',
    'nav_add_favourites'    => 'Add to Favourites',
    'nav_on_fb'             => 'on Facebook',
    'nav_contact'           => 'Contact',
    'nav_sell'              => 'Sell',
    'nav_bin'               => 'Buy It Now',
    'nav_moresearches'      => 'More',
    'nav_sitemap'           => 'Sitemap',
    'nav_blog'              => 'Blog',
    'nav_superdeals'        => '<strong class="deals">Super Deals!!!</strong>',
    'nav_faq'               => 'FAQ',
    

    #mobile nav
    'm_nav_home'            => 'Home',
    'm_nav_local'           => 'Local',
    'm_nav_misspelled'      => 'Misspelt',
    'm_nav_unwanted'        => 'Unwanted',
    'm_nav_nightowl'        => 'Night Time Bargains',
    'm_nav_bin'             => 'Buy It Now',
    'm_nav_buynow'          => 'Buy It Now',

    #titles
    'title_about'           => 'About Us',
    'title_index'           => 'Crazy Local Bargain Search',
    'title_home'            => 'Crazy Local Bargain Search',
    'title_faq'             => 'Frequently Asked Questions',
    'title_manchester'      => 'Cheap Stuff Near Manchester',
    'title_local'           => 'Crazy Local Bargain Search',
    'title_misspelled'      => 'Misspelt Keyword Bargain Search',
    'title_rightnow'        => 'Auction Listings Ending Now With Zero Bids',
    'title_nightowl'        => 'Night Time Bargain Search',
    'title_unwanted'        => 'Unwanted Gifts on eBay',
    'title_furniture'       => 'Cheap Furniture In your Town',
    'title_joblot'          => 'Job-lots For Free Pick-up In Your Town',
    'title_baby'            => 'Baby Bargain Search',
    'title_laptop'          => 'Local Laptop Bargains',
    'title_carparts'        => 'Local Car Part Bargains',
    'title_cars'            => 'Local Cars at Great Prices',
    'title_buynow'          => 'Buy It Now Search',
    'title_contact'         => 'Crazy `Contact Us` Page',
    'title_privacy'         => 'Privacy Policy Page',
    'title_amazon'          => 'Amazon Search Page',
    'title_moresearches'    => 'All our searches on one page!',
    'title_map'             => 'Directions Service',
    'title_sell'            => 'Sell your item on eBay',
    'title_bin'             => 'Buy It Now From Selected Sellers With Proven Reliability and Solid Returns Policy',
    'title_error'           => 'Error Page',

    #bookmark texts
    'bookmark_about'        => 'About Us',
    'bookmark_index'        => 'BC Local Bargain Search',
    'bookmark_home'         => 'BC Local Bargain Search',
    'bookmark_faq'          => 'BC Frequently Asked Questions',
    'bookmark_manchester'   => 'BC Manchester Bargain Search',
    'bookmark_local'        => 'BC Local Bargain Search',
    'bookmark_misspelled'   => 'BC Misspelt Bargain Search',
    'bookmark_rightnow'     => 'BC Ending Now Bargain Search',
    'bookmark_nightowl'     => 'BC Crazy Time Bargain Search',
    'bookmark_unwanted'     => 'BC Unwanted Gift Search',
    'bookmark_furniture'    => 'BC Furniture Bargain Search',
    'bookmark_joblot'       => 'BC Joblot Bargain Search',
    'bookmark_baby'         => 'BC Baby Bargain Search',
    'bookmark_laptop'       => 'BC Laptop Bargain Search',
    'bookmark_carparts'     => 'BC Car Parts search',
    'bookmark_cars'         => 'Local Cars at Great Prices',
    'bookmark_contact'      => 'BC Contact Us Page',
    'bookmark_privacy'      => 'BC Privacy Policy',
    'bookmark_amazon'       => 'BC Amazon Search',
    'bookmark_buynow'       => 'BuyNow Search',
    'bookmark_moresearches' => 'BC More Searches',
    'bookmark_map'          => 'BC Directions Service',
    'bookmark_switchcountry'=> 'BC Switch Country',
    'bookmark_newsletter'   => 'BC Newsletter',
    'bookmark_settings'     => 'BC Settings Manager',
    'bookmark_sell'         => 'Start Selling on eBay',
    'bookmark_bin'          => 'Buy It Now From Selected Sellers',
    'bookmark_vintage'      => 'BC Vintage Search',
    'bookmark_deals'        => 'Great Deals',
    'bookmark_superdeals'   => 'Great Deals',
    'bookmark_error'        => 'BC Error Page',
    'bookmark_outlink'      => 'BC Outlink Page',

    #write_content
    'write_content_h2'      => 'Find Under Priced Items Listed on eBay!',
    'write_content_p'       => 'We help you find \'treasure\' '.
                                'hidden on eBay - Crazy bargains where the '.
                                'listing has common mistakes(won\'t ship, '.
                                'misspellings etc.), letting you buy '.
                                'it far below it\'s true value!',
    #try search functions
    'try_nightowl'          => 'Try the \'Night Time Bargain\' search now!',
    'try_furniture'         => 'Try the \'Local Furniture\' search now!',
    'try_baby'              => 'Try the \'Baby Bargains\' search now!',
    'try_local'             => 'Try the \'Local Bargain\' search now!',
    'try_rightnow'          => 'Try the \'Ending Now\' search now!',
    'try_unwanted'          => 'Try the \'Unwanted Gift\' search now!',
    'try_joblot'            => 'Try the \'Job-Lot\' search now!',
    'try_misspelled'        => 'Try the \'Misspelt Title\' search now!',
    'try_laptop'            => 'Try the \'Local Laptops\' search now!',
    'try_rightnow'          => 'Try the \'Ending Now\' search now!',
    'try_carparts'          => 'Try the \'Car Parts\' search now!',
    'try_cars'              => 'Try the \'Car\' search now!',
    'try_bin'               => 'Try the \'Buy It Now\' search now!',
    'try_buynow'            => 'Try the \'Buy It Now\' search now!',

    #Cross Sell Text
    'cross_sell_local_title'             => 'Local',
    'cross_sell_local_description'       => 'Find bargains near you, that won\'t fit in the mail!',

    'cross_sell_furniture_title'         => '<span class="hidden-xs">Local </span>Furniture',
    'cross_sell_furniture_description'   => 'Find tables, chairs, sofas and large appliances',

    'cross_sell_unwanted_title'          => 'Unwanted <span class="hidden-xs">Gifts</span>',
    'cross_sell_unwanted_description'    => 'Find people\'s unwanted gifts, at used prices!',

    'cross_sell_cars_title'              => 'Local Cars',
    'cross_sell_cars_description'        => 'Find your next car, nearby, at a great price!',

    #common items
    'quick_search'          => 'Search Now!',

    'desc_short_moresearches_title' => 'More Searches',

    #descriptions


    #home
    'desc_long_home_title' => 'Buy It Right Using our Bargain Searches',
    'desc_long_home1'      => 'Our bargain searches make it easy to grab the '.
                                'best  buys on the Internet. If you are crazy'.
                                'about Internet bargains, but going crazy trying to find '.
                                'them, you are in the right place!',
    'desc_long_home2'      => "<form action='/unwanted/search/' id='unwanted_form' name='multisearch_form' '.
                            'method='get' class='rightnowsearchform' style='margin-left:15px;'><fieldset>
<span id='switchinput'>Postal Code: <input type='text' name='postcode' id='postcode' class='text default' /></span>
<select id='page2' name='page'>
<option value='local'>Local Bargains</option>
<option value='unwanted'>Unwanted Gifts</option>
<option value = 'nightowl'>Night Time</option>
<option value = 'rightnow'>Ending Now</option>
</select>
<input type='submit' onclick=\"recordOutboundLink(this, 'unwanted', 'search');\" value='Search Now'/>
</select>",
    'desc_long_home3'      => 'Our specific bargain searches take you  straight to the '.
                                'items you want to see. One click finds eBay listings '.
                                'that  <a href="/rightnow">end right NOW</a>,  '.
                                '<a href="/nightowl">night time specials</a>,  '.
                                '<a href="/misspelled">misspelled bargains</a>, '.
                                '<a href="/unwanted">unwanted gifts</a>,'.
                                '<a href="/local">local bargains</a>, or  click to '.
                                'find super savings at <a href="/amazon">Amazon.com</a>'.
                                '.  How easy is that?  Our bargain searches put the '.
                                'best Internet buys at your finger tips.',
    'desc_long_home4'      => 'Ready to go? Start now by clicking one of our quick '.
                                'search options below',
    'desc_long_home5'      => '<ul><li>Use Our searches to save shipping costs and '.
                                'get great deals on <a href="/local">local bargains</a>'.
                                ' at eBay.  Our helpful tips let you zero right '.
                                'in on the local savings.</li>'.
                                '<li><a href="/misspelled">Misspelled listings</a> '.
                                'often have few bids. If you can spell it right, you can '.
                                'snag some real savings.</li>'.
                                '<li>A white elephant in <a href="/unwanted">'.
                                'unwanted gifts</a> may be the exact piece that '.
                                'completes your collection.</li>'.
                                'Our <a href="/nightowl">night time</a> and '.
                                '<a href="/rightnow">ending NOW searches</a>'.
                                '  put you  in the right place at the right time  to cash '.
                                'in on the Internet\'s best bargains.</li></ul>',
    'desc_long_home6'      => 'We don\'t collect personal information from our visitors. '.
                                'Our free searches simply take you directly to '.
                                'Internet bargains. We are proud to be a certified eBay '.
                                'compatible application. Get the <a href="/content/faq">FAQs</a> '.
                                'here or click to learn more about our website.',

    #aboutus
    'desc_short_about_title'=> 'About Us',
    'desc_long_about_title' => 'About Us',
    'desc_long_about1'      => 'Tom and Bry, co-founders and co-owners of this site have '.
                                'something in common. They are both <strong>crazy</strong>'.
                                ' about bargain hunting! In 2008, they put their heads '.
                                'together and created a unique range of search engines that'.
                                ' track down the best bargains at eBay and Amazon.com.',
    'desc_long_about2'      =>  '<h3>Our searches flush out the web\'s best buys</h3>',
    'desc_long_about3'      =>  'Everybody loves a bargain, but searching the web... '.
                                'well, it\'s a jungle out there! Our searches '.
                                'are especially designed to locate bargains that are '.
                                'easily lost in the shuffle. Our searches are an eBay '.
                                'approved application, inspected by eBay and found '.
                                'to meet their quality standards.',
    'desc_long_about4'      =>  'Bargain hunting can turn the inexperienced shopper '.
                                'into a basket case. Our searches are tuned to '.
                                'make it easy for our visitors to load up their '.
                                'baskets with the best buys on the web. Yet, while '.
                                'laid-back for the novice searcher, our searches are '.
                                'top-drawer. Our advanced search functions, like '.
                                'those in our popular '.
                                '<a href="/local">local search</a>, give the '.
                                'dedicated bargain hunter all the facilities needed '.
                                'to bag the big bargains.',
    'desc_long_about5'      =>  '<h3>Find hidden bargains as you stay in the blind</h3>',
    'desc_long_about6'      =>  'We puts special focus on searches that'.
                                ' many shoppers overlook, like those '.
                                '<a href="/rightnow">ending NOW</a>,'.
                                '<a href="/nightowl">night time listings</a>'.
                                ', and <a href="/misspelled">misspelled bargains</a>'.
                                ', the types of searches that help eBay and Amazon '.
                                'shoppers save like crazy!',
    'desc_long_about7'      =>  'Because we don\'t sell anything, registration '.
                                'is unnecessary. You remain anonymous as you hunt '.
                                'for the web\'s best buys. If you need to enter a '.
                                'postcode, it is only to successfully complete a '.
                                'local search and we only collect your email address '.
                                'if you contact us. Get the full details in '.
                                'our <a href="/content/privacy">privacy policy</a>.',
    'desc_long_about8'      =>  'We keep the fun in searching for bargains. Pick a '.
                                'search from our top navigation or click to use our '.
                                'quick bargain searches and go bargain hunting '.
                                'crazy!',

    #privacy
    'desc_long_privacy_title' => 'Privacy Policy',
    'desc_long_privacy1'      => '<em>Our privacy policy is quite simple.  '.
                                    'We absolutely respect your privacy!</em>',
    'desc_long_privacy1'      => '<h3>Site Objective</h3>',
    'desc_long_privacy2'      => 'This website exists to help users to find bargains on third '.
                                    'party websites (such as eBay), for which we receive a '.
                                    'commission. This is an independent search site and '.
                                    'is not a part of eBay.',
    'desc_long_privacy3'      => '<h3>Privacy Principles</h3>',
    'desc_long_privacy4'      => 'We do not require any <a href='.
                                    '"http://en.wikipedia.org/wiki/Personally_identifiable_information"'.
                                    ' target="pii">\'personally identifiable information\'</a>',
    'desc_long_privacy5'      => '<h3>Use of Cookies</h3>',
    'desc_long_privacy6'      => 'Cookies allow us to remember your settings, such as language, '.
                                    'your postcode, and your last used search term. We store '.
                                    'cookies in order to allow the site to function smoothly. '.
                                    'If you wish to opt out of cookies, refer to <a href="'.
                                    'http://www.aboutcookies.org.uk/managing-cookies">aboutcookies.org.uk'.
                                    '</a> which contains information on how to disable cookies in all '.
                                    'major web browsers. Disabling cookies may harm the usability '.
                                    'of this site.',

    #sell
    'desc_long_sell_title' => 'Selling on eBay',
    'desc_long_sell1'      => '<p>We are an independent search site which operates as a benefit '.
                            'to bargain hunters. We do not currently offer any services to successful sellers. '.
                                  'All of the items which appear in our search results are listed either '.
                            'on eBay, or on Amazon web sites. We have no part to play in the buying '.
                            'or selling process. All listing, buying and selling is done on eBay or '.
                            'Amazon and is subject to their rules.</p>'.
                                  '<p>As a seller, you should really be disappointed if your items '.
                            'appear on our search results, because that would indicate that you have restricted '.
                            'your market and will be destined to sell for a low price.'.
                                  'We are a site, mainly there to help buyers to find bargains.</p>'.
                                  '<p>We list some items which are currently on sale on eBay or Amazon, '.
                            'and we try to select items which are destined to reach an unusually low price.</p>'.
                                  '<p>If you want to sell on eBay, for a good price, you must write your eBay '.
                            'listing well and our bargain hunters will not see it in our results. '.
                            'If you write your eBay listing badly, or restrict your market in some way, '.
                            'then your item might appear, but don\'t expect it to sell '.
                            'for a good price. Items appearing in our search results are there for the '.
                            'bargain hunting buyers\' benefit.</p>',
    'desc_long_sell2'      => '<p>However, we realise that some of our users just wish to sell '.
                            'locally and the selling price is not overly important.',

    'desc_long_sell3'      => 'So. If you really do only want to sell as \'cash on collection only\' '.
                            'then what you need to do is list it on eBay. You should put the exact words '.
                            '\'cash on collection\' into the description part of your listing and choose the '.
                            'local collection shipping option. That might make your listing visible '.
                            'to people in your local area.',

    'desc_long_sell4'      => 'We do not charge anything for displaying items in our '.
                            'search results and it does not guarantee that any item will appear.',
    'desc_long_sell5'      => 'When an item has received a few bids, or when our software stops '.
                            'considering it to be at a bargain price, then the item may stop showing in '.
                            'our search results. It will, of course still appear in regular ebay results.',

    'desc_long_sell6'      => 'If you are new to eBay, it might feel a bit daunting to set up an account '.
                            'and list your items for sale. However, eBay have now made it quite simple '.
                            'and they provide helpful step by step guides.',
    'desc_long_sell7'      => 'First you need a regular eBay account. Skip this step if you already have '.
                            'an eBay account. <a href ="/outlink/redirect/page/signup/" target="signup">'.
                            'Click here to sign up for an eBay account.</a>',
    'desc_long_sell8'      => 'If you wish to sell your first item, then you may need to upgrade your '.
                            'eBay account to a \'seller account\'. '.
                            '<a href ="/outlink/redirect/page/startselling/" target="startselling">'.
                            'Click here to start selling.</a>',
    'desc_long_sell9'      => 'You might be asked to verify your identity by, perhaps, entering a phone number. '.
                            'But it\'s all pretty straightforward. Just follow the prompts.'
                              .' If you are going to sell lots of items, you might choose to set up a '.
                            'paypal account. But that is not immediately essential'
                              .' Then you need to decide whether you wish to sell at a fixed price, '.
                            'or sell in an auction style listing. There are a few decisions to make, and for now, '.
                            'we suggest you try an auction style listing. But set your '.
                            'starting price at the very minimum that you will accept'
                              .' You should prepare a photo of your item for sale, and you can usually '.
                            'do that with a mobile phone or digital camera. For some items, you can '.
                            'get a stock photo from the internet.',

    #contact
    'desc_long_contact_title' => 'Contact Us.',
    'desc_long_contact1'      => 'Thank you for visiting. We hope you have enjoyed using '.
                                    'our Searches. ',
    'desc_long_contact2'      => 'Please use the contact form below to let us know how well our search'.
                                    ' features did in helping you find eBay and Amazon.com bargains.',
    'desc_long_contact3'      => '<ul><li>How successful was your search experience? We always '.
                                    'appreciate success stories!</li>'.
                                    '<li>What can we improve? Let us know and we\'ll work on it.</li>'.
                                    '<li>Did you find broken or misdirected links or typos on our '.
                                    'website? Was there anything that just didn\'t work? '.
                                    'Let us know and we\'ll fix it quick!</li>'.
                                    '<li>Do you have a question that our <a href="/content/faq">FAQs</a> '.
                                    'didn\'t address? Ask us and we\'ll do our best to answer '.
                                    'and maybe add your question to our FAQs..</li></ul>',
    'desc_long_contact4'      => 'Remember, you never need worry about leaving your information '.
                                    'with us. We absolutely respect your privacy and will never use '.
                                    'information you leave with us for any use other than you permit. '.
                                    'You can <a href="/content/privacy">read our complete Privacy Policy here</a>.',

    'desc_long_contactinfo'    => 'You may also contact us by email at '.
                                    '<a href="mailto:help@baycrazy.com">help@baycrazy.com</a>.</br>'.
                                    'BayCrazy.com is a domain name operated by BayCrazy Ltd, a company registered in '.
                                    'England and Wales. Company number 07820359 '.
                                    'our registed address is:<br>BayCrazy Ltd.<br />'.
                                    'Willow House<br />The Redstone Centre<br />'.
                                    'Alsager<br />ST7 2AU<br />United Kingdom',

    #faq
    /*
    'desc_long_faq_title' => 'Frequently asked questions',
    'desc_long_faq1'      => 'If you have any questions about this website or need help using the site, '.
                                    'then please take a look at the following frequently asked questions.',
    'desc_long_faq2'      => 'To reveal the answer to any question, just click on it.',
    'desc_long_faq3'      => 'If you cannot find your answer here, then please visit '.
                            'our <a href="/contact/contact">Contact Us</a> page.',
    'faq_q1'              => 'Q: How do I sell on here?',
    'faq_a1'              => 'A: You don\'t! this website provides search features to help '.
                                'buyers to find underpriced bargains which are listed on eBay. We '.
                                'don\'t directly accept listings.',
    'faq_q2'              => 'Q: How do I join?',
    'faq_a2'              => 'A: You don\'t! There\'s no need to join at this website '.
                            'and we don\'t need to know who you are. '.
                                 'Simply use <a href="www.bayCrazy.com">our search features.</a> '.
                                 'You may need to sign up with eBay or Amazon to get the best use of this website.',
    'faq_q3'              => 'Q: What does it cost to use this website?',
    'faq_a3'              => 'A: Nothing! We do not charge anyone to use our searches. ',
    'faq_q4'              => 'Q: I listed some items on eBay. Why don\'t they all appear on your search results?',
    'faq_a4'              => 'A: If it looks like an item will sell for a bargain price, '.
                            'then we might choose to list it: '.
                                 'If it looks likely to sell well, then we probably will not show it. '.
                            'It\'s not just about location.',
    'faq_q5'              => 'Q: Some items I was watching or bidding on have disappeared from your search results. '.
                            'Where did they go?',
    'faq_a5'              => 'A: We may stop showing items if they start to '.
                            'look like they will sell at a decent price. '.
                                 'If you have bid on those items, then log into eBay '.
                            'directly and check your \'my eBay\' page, '.
                                 'and check for emails from eBay in the inbox of '.
                            'your eBay registered email account.',
    'faq_q6'              => 'Q: I\'ve made an incorrect bid on an item. What should I do?',
    'faq_a6'              => 'A: You must immediately use eBay\'s procedure as '.
                                'described <a href="http://pages.ebay.co.uk/help/buy/questions/'.
                                'retract-bid.html" target="_blank">here.</a>',
    'faq_q7'              => 'Q: I bid on a local item and now I find that the buyer '.
                            'will not allow me collect it. What shall I do?',
    'faq_a7'              => 'A: Bidding on the eBay site it is between you and eBay '.
                            'and the seller, subject only to eBay\'s rules. '.
                                 'You should always read the full listing. Currently, '.
                            'we suggest taking any dispute up with the seller '.
                                 'using <a href="http://resolutioncentre.ebay.co.uk/" target="resolution">'.
                            'eBay\'s Resolution Centre.</a>',
    'faq_q8'              => 'Q: How can this site be free? What\'s the catch?',
    'faq_a8'              => 'A: This website has always been free to use. '.
                            'The site receives modest commissions for sending buyers '.
                                 'and new members to eBay and Amazon. There\'s no catch. ',
    'faq_q9'              => 'Q: The local search tells me that my postcode is wrong. Why is that?',
    'faq_a9'              => 'A: You are almost certainly entering a postcode with a number '.
                            'zero where a letter \'O for orange\' should be, or '.
                                 'entering a letter \'O for orange\' where a number zero should be. Or, '.
                            'you may have the wrong country selected. Check the country '.
                                 'flag setting towards the top right of any of our pages.',
    'faq_q10'              => 'Q: I do a search but there are no results showing. Why is that?',
    'faq_a10'              => 'A: Make sure that you are not searching with an unusual or misspelled keyword. '.
                                  'Make sure you haven\'t accidentally put your '.
                            'postcode in the search term keyword box. '.
                                  'Also make sure that you are searching in '.
                            '\'All categories\' or the correct category. '.
                                  'Try increasing the amount you might be prepared to bid, '.
                            'or the distance you will travel to collect.',
    'faq_q11'              => 'Q: The distances you show are wrong. Why is that?',
    'faq_a11'              => 'A: We show approximate distances \'as the crow flies\' '.
                            'between you and the seller. The distance by road might be different.',
    'faq_q12'              => 'Q: Can you tell me more about an item that you have for sale?',
    'faq_a12'              => 'A: We do not have any items for sale! We show you items '.
                            'listed on eBay or Amazon. Contact the seller there.',
    'faq_q13'              => 'Q: I\'d like to buy one of your items as a \'Buy It now\'. How can I do that?',
    'faq_a13'              => 'A: We do not have any items for sale! '.
                            'We show you items listed on eBay or Amazon. Contact the seller there.',
    'faq_q14'              => 'Q: I bought an item, but have had no contact with the seller. '.
                            'How do I pay?  Where is my item?',
    'faq_a14'              => 'A: We show you items listed on eBay or Amazon. Contact the seller there. '.
                                  'Usually you can do that by clicking on the seller\'s user identity.',
    'faq_q15'              => 'Q: I can\'t log into this website. What should I do?',
    'faq_a15'              => 'A: This website does not have or use any log in process! '.
                                 'If you mean that you can\'t log into your eBay account, '.
                            'then you should use their processes. '.
                                 'We understand that the page you need is '.
                            '<a href="https://signin.ebay.co.uk/ws/eBayISAPI.dll?SignIn" target="ebaylogin">here</a>',
    'faq_q16'              => 'Q: My account has been hacked. What can I do?',
    'faq_a16'              => 'A: This website does not have or use accounts! '.
                                 'If you mean that your eBay account has been hacked, '.
                            'then you need to contact eBay as a matter of urgency! '.
                                 'We understand that you can currently do that from '.
                                 '<a href="http://ocs.ebay.co.uk/ws/eBayISAPI.dll?CustomerSupport" '.
                            'target="hackedebay">this link.</a>',

    'faq_q17'              => 'Q: Who runs this website? Is it a part of eBay?',
    'faq_a17'              => 'A: This is an independent search website. '.
                            'It is owned by BayCrazy Ltd.  A UK registered company, '.
                                 'registration number 07820359. BayCrazy.com is NOT owned or operated by eBay',
    'faq_q18'              => 'Q: What countries does this website operate in?',
    'faq_a18'              => 'A: We are based in the UK. It currently allows you to '.
                            'search for items which are listed on the eBay sites '.
                                 'of the UK and Northern Ireland, United States of America, Germany, and Australia. ',
    'faq_q19'              => 'Q: Are your searches safe to use?',
    'faq_a19'              => 'A: Absolutely! We operate under very strict rules including UK law '.
                            'applicable to registered companies, and is closely monitored '.
                            'by eBay Inc. and their UK agents ROEYE. ',
    'faq_q20'              => 'Q: Will you ever send me emails or cause me to receive '.
                            '\'spam\' unsolicited emails?',
    'faq_a20'              => 'A: This website does not currently collect the email addresses of our users. '.
                                 'If at any time, we do get to know your email address, '.
                            'then we would only normally contact you with your permission '.
                                 'or to give you information which you have requested. '.
                                 'Very rarely we might contact our users to conduct limited market research. '.
                                 'If you ever tell us that we should not contact you, '.
                            'then we would honour your request.',
    'faq_q21'              => 'Q: Does this website use cookies?',
    'faq_a21'              => 'A: This site does use cookies, only so that we can provide you '.
                            'with the best service possible. '.
                                 'E.g. we may store your postcode and preferred spending amount. '.
                            'We don\'t share those cookies. '.
                                 'The sites which we link to, e.g. eBay.com, may also use cookies, '.
                            'but that is subject to their own cookie policies. ',
    'faq_q22'              => 'Q: What data do you store about me '.
                            'or my use of this website or eBay or Amazon?',
    'faq_a22'              => 'A: We may store those pieces of information '.
                            'which you enter on forms on our website, e.g. your Postcode. '.
                                'Like most websites, we will also record the time and date '.
                            'of any page you visit on our website and your '.
                                'internet address at the time of your visit. '.
                                'Your browser software will identify its version to us, so we might know, '.
                            'for instance, whether you are using a mobile device. '.
                                'This does not identify you personally and is only used so that '.
                            'we can monitor and manage the effectiveness of our service to you. '.
                                'We will not see or store anything which you do on eBay '.
                            'or other websites which we may connect you to. '.
                                'From time to time, eBay or Amazon may give us '.
                            'limited data related to which items were bid on or bought by our users, '.
                                'but they will not give us personally identifiable information about our users. ',
    'faq_q23'              => 'Q: I have another problem with the website. What should I do?',
    'faq_a23'              => 'A: Please contact us directly using our '.
                            '<a href="/contact/contact">Contact Us</a> page.'.
                                 ' Please describe your problem fully and it would help '.
                            'if you would tell us your postcode and country. '.
                                 'We can only reply if you enter your correct email address. '.
                            'We cannot identify you from your eBay User identity, '.
                                 'so make sure you tell us your email address in every case.',
    'faq_q24'              => 'Q: I have some suggestions for changes to your site. How can I contact you?',
    'faq_a24'              => 'A: We love feedback and suggestions. '.
                            'Please contact us directly using our <a href="/contact/contact">Contact Us</a> page.',
    */

    #local
    'desc_long_local_title' => 'CrazyLocals : Local eBay Bargain Search',
    'desc_long_local1'     => 'Are shipping costs driving you crazy? Today\'s high shipping '.
                                'costs can change a low-bid purchase into a budget breaker. '.
                                'When you use our various local searches to help you find local '.
                                'bargains, you eliminate the need to pay for shipping as well '.
                                'as the worry about shipping errors or  damage.',
    'desc_long_local2'     => 'Although you will have purchased your item before you can '.
                                'collect it, shopping locally lets you examine your purchase '.
                                'to see that it meets your expectations and that it is free '.
                                'from unexpected defects before you take possession.',
    'desc_long_local3'     => 'Local searches are often used to find large, bulky items but, '.
                                'they are also the ideal way to purchase fragile items such '.
                                'as collectibles and glassware. Because you are in charge of '.
                                'transport you can supervise the loading of your bargain and '.
                                'ensure it is securely packed for a safe journey home.',
    'desc_long_local4'     => 'Our Crazy local search is unique in that we help you find the '.
                                'items listed as "pick up" or "collect in person only" as '.
                                'well as the items where a seller tries to offset price with '.
                                'high shipping costs. Use local search to find items listed '.
                                'in several categories. Click a link below for category '.
                                'details or just enter your postcode in our quick search to '.
                                'browse the complete range of available items.',
    'desc_long_local5'     => '<ul class=\'locallist\'>'.
                                '<li><a href="/cars">Local Cars</a></li>'.
                                '<li><a href="/carparts">car parts</a></li>'.
                                '<li><a href="/furniture">Furniture</a></li>'.
                                '<li><a href="/joblot">Job lots</a></li>'.
                                '<li><a href="/baby">Baby bargains</a></li>'.
                                '<li><a href="/laptop">Laptops</a></li></ul>',

    'desc_short_local_title'=> 'Local Bargain Search',
    'desc_short_local1'     => 'Find Furniture, Pc\'s, TVs, Exercise Equipment, Alloy '.
                                'Wheels and more at lower prices on eBay',
    'desc_short_local2'     => 'These are often heavy, fragile or bulky '.
                                'listed items the seller probably doesn\'t want to send by mail,'.
                                'but where it\'s physically close to you. The seller has severely '.
                                'restricted their market and will get very few bidders. '.
                                'Just go round to their house and pick the item up for pennies.',
    'desc_tiny_local'       => 'Bargains close to you!',

    'desc_tabs_local'       => '<h2>Local Bargain Search</h2>
        <p>Search Now for bargains near you, which either can\'t be
        sent in the mail, or will cost loads! So they\'ll probably
        go dirt cheap.</p>
        <h3>Save Time, Buy Local</h3>
        <p>Just enter your postcode and search now</p>',

    'formhelp_local'        => 'Just fill in your postcode...',
    'checklisting_local'    => 'Not all items are available for collection! Always check the full listing!',

    #misspelled
    'desc_long_misspelled_title'=> 'Misspelled Titles',
    'desc_long_misspelled1'     => 'Misspelled titles can be a real problem for both '.
                                    'eBay sellers and you. Some sellers just cannot '.
                                    'spell. Others try to be original with their titles, '.
                                    'adding characters and abbreviations that are '.
                                    'perfectly understandable when visible, but hide '.
                                    'them from a properly spelled search.',
    'desc_long_misspelled2'     => 'To err is human, and when sellers misspell the '.
                                    'titles of their eBay listings, you will often find '.
                                    'some really divine bargains! If you are looking to '.
                                    'buy at rock bottom prices, misspelled titles may '.
                                    'be just the eBay listings that fit the bill and '.
                                    'our custom misspelled title search makes '.
                                    'finding them a no-brainer!',
    'desc_long_misspelled3'     => 'Spelling errors make a seller\'s eBay listings '.
                                    'nearly invisible, which results in no-bid, low-bid '.
                                    'items. We say <em>nearly</em> invisible because the '.
                                    'misspelled title search puts you just a '.
                                    'click away from these bargain basement items.',
    'desc_long_misspelled4'     => 'Always a ground-breaker (and a treasure-hunter), we '.
                                    'dig deep to find misspelled titles and we make the '.
                                    'corrections so that you can hit pay dirt on your '.
                                    'first try. All you need do to find a misspelled '.
                                    'bargain is use our simple misspelled search '.
                                    'and spell it right!',
    'desc_long_misspelled5'     => 'If you\'re not sure that you\'ve spelled it right, '.
                                    '<a href="http://dictionary.reference.com/">'.
                                    'check your spelling here</a>. (After all, anyone '.
                                    'can make a <strike>misteak</strike> mistake!)',

    'desc_short_misspelled_title'=> 'Misspelling Search',
    'desc_short_misspelled1'     => 'Find eBay listings with typos in the title',
    'desc_short_misspelled2'     => 'When the seller misspells some crucial word in the title. Other '.
                                     'buyers never get a look in, so you may be the only bidder and '.
                                     'walk away with a bargain. :o) But remember,'.
                                     '<span class="red"> YOU must spell it '.
                                     'CORRECTLY! :o) </span>',

    'desc_tiny_misspelled'      => 'Find items normal searches never see!',

    'desc_tabs_misspelled'      => '<h2>Misspelled Bargain Search</h2>
        <p>We take the hard work out of finding misspelled bargains, saving you loads of time and energy.</p>
        <h3>Save time, grab a misspelled bargain!</h3>
        <p>Just spell it correctly, and click search now!</p>',

    'formhelp_misspelled'       => 'Just enter your (correctly spelled) searchterm!',

    #rightnow
    'desc_long_rightnow_title'  => 'Zero Bid Auctions Ending NOW!',
    'desc_long_rightnow1'       => 'Ready for some fast-paced bargain buying? Ending'.
                                    ' NOW is the place to get a head start on huge '.
                                    'savings! The clock is ticking and time is '.
                                    'running out for these sellers. The auctions '.
                                    'below are ending right now! With ZERO BIDS !',
    'desc_long_rightnow2'       => 'More, these sellers are most likely anxious '.
                                    'to see that first bid. It may be their only '.
                                    'one! This is your chance to cash in on a '.
                                    'really big bargain!',
    'desc_long_rightnow3'       => 'Check out the listings below. We\'ve run '.
                                    'down some of the best ones so that you can '.
                                    'quickly cherry-pick the savings. All end '.
                                    'within minutes and all show '.
                                    '<strong>Zero Bids</strong>! One mad dash '.
                                    'between you and the clock might result '.
                                    'in treasure for you that other buyers overlooked.',
    'desc_long_rightnow4'       => 'This is a grab bag of great deals at '.
                                    'bargain prices! Pick up the pace with '.
                                    'our quick search that finds even '.
                                    'more Ending NOW bargains in your favorite '.
                                    'category at the price you are willing to '.
                                    'pay! Don\'t wait! Latch on while you can! '.
                                    'It\'s you against the clock!',
    'desc_long_rightnow5'       => 'If you are tired of bargain hunting and '.
                                    'ready for bargain buying, your time is '.
                                    'now! Don\'t hesitate. (Are you STILL '.
                                    'reading this?) Get started now!',

    'desc_short_rightnow_title' => 'Ending Now!',
    'desc_short_rightnow1'      => 'Check out these auctions ending RIGHT NOW! '.
                                    'With Zero Bids on eBay!',
    'desc_short_rightnow2'      => 'One mad dash between you and the '.
                                    'clock may result in treasure for you that other buyers overlooked.'.
                                    'Check out these auctions ending RIGHT NOW!',

    'desc_tiny_rightnow'        => 'Last few minutes, with no bids!',

    'desc_tabs_rightnow'        => '<h2>Save Time, and Money,
        with auctions ending Right Now!</h2>
        <p>Our Ending Now search gives you the items ending *now*
        (okay, you might have a minute or two), so you save time by
        only seeing items with no bids, and low prices</p>
        <h3>So why wait?</h3>
        <p>Click \'Search Now\' and find some bargains!</p>',

    'formhelp_rightnow'         => 'Just click search and enjoy!',

    #nightowl
    'desc_long_nightowl_title'  => 'Night Time Specials',
    'desc_long_nightowl1'       => 'Can\'t sleep? That\'s great! Night Time Specials'.
                                    ' is the perfect page for both early birds and'.
                                    'insomniacs. Why catch zzzz\'s when you can '.
                                    'catch the biggest bargains around? This is '.
                                    'the stuff dreams are made of! Our NightOwl '.
                                    'search identifies the end time of each Night '.
                                    'Time Special. You can pop your bid in before '.
                                    'bedtime and maybe wake up to a bargain!',
    'desc_long_nightowl2'       => 'Don\'t ask us why, \'cause we don\'t know,'.
                                    ' but some sellers think the dead of night is the '.
                                    'best time to end a sale. It\'s your chance to ask '.
                                    'for the moon and get it. The magic starts at '.
                                    'midnight, but the big stars start popping at '.
                                    'about 3a.m. and they shine all night long! '.
                                    'These early morning hours mean fewer bids, '.
                                    'lower prices, and bigger bargains than at '.
                                    'any other time of the day.',
    'desc_long_nightowl3'       => 'So, put away that sleepy-time tea and grab '.
                                    'your coffee mug. You are going to want to be '.
                                    'awake to cash in on these sleeping giants. '.
                                    'We\'ve made finding the big bargains so easy '.
                                    'that you could find them in your sleep! But, '.
                                    'this isn\'t a dream. It\'s the real deal! ',
    'desc_long_nightowl4'       => 'Take a moment and scroll down the listings to'.
                                    ' see just a few of the great items on sale '.
                                    'now or use the search at left to find more '.
                                    'bargains by time , category, price or keyword.',

    'desc_short_nightowl_title' => 'Night Time Bargains',
    'desc_short_nightowl1'      => 'Items selling on eBay while others sleep!',
    'desc_short_nightowl2'      => 'Search out listings where the seller has '.
                                    'ended their auction in the dead of night when '.
                                    'no-one is watching or bidding. The fewer bidders, the lower the price.',
                                    'Just place a bid before bed-time and wake '.
                                    'up to a bargain.',

    'desc_tiny_nightowl'        => 'Items selling while others sleep!',

    'desc_tabs_nightowl'        => '<h2>Night Time Bargain Search</h2>
        <p>Search Now for bargains ending in the dead of night, nobody else will be around to top your bid,
        so even if you bid early and go to bed, your chances of winning are good!</p>
        <p>Just click \'search now\', and grab a bargain!</p>',

    'formhelp_nightowl'         => 'Just search, and see what you find!',

    #unwanted
    'desc_short_unwanted_title' => 'Unwanted Gifts',
    'desc_short_unwanted1'      => 'eBay Listings where sellers turn unwanted gifts'.
                                    ' into cold, hard cash.',
    'desc_short_unwanted2'      => 'Thest just want to sell their '.
                                    'gift, opening the door for you, the canny buyer, '.
                                    'to catch hold of undervalued treasure!',

    'desc_long_unwanted_title' => 'Unwanted Gifts',
    'desc_long_unwanted1'      => 'Although Unwanted Gifts are easily among the most '.
                                    'overlooked eBay listings, this category contains '.
                                    'some of the best bargains on eBay!  Sellers may '.
                                    'not know or care about the original price of '.
                                    'these items, giving you the chance to step in and'.
                                    ' rescue these under-valued  and unwanted gifts '.
                                    'for far less than cost.',
    'desc_long_unwanted2'      => '"You shouldn\'t have." Sometimes when we open a '.
                                    'gift we mean exactly what we say. Sooner or '.
                                    'later, we all receive that extra toaster, the '.
                                    'wrong-sized bedding or clothing item - an '.
                                    'unwanted gift. What might be the wrong-size, '.
                                    'wrong-color, wrong-style, or unneeded extra '.
                                    'for a seller might be just the big bargain '.
                                    'that perfectly fits your needs as well as '.
                                    'your budget.',
    'desc_long_unwanted3'      => '<strong>Brand new items at bargain basement '.
                                    'prices!</strong> The beauty of a bargain '.
                                    'is often in the eye of the beholder! Most '.
                                    'unwanted gifts are brand new with tags '.
                                    'still attached, never used, never opened '.
                                    'but for the gift wrap.',
    'desc_long_unwanted4'      => 'When sellers clean out their closets, you '.
                                    'can clean up on value-for-dollar savings. '.
                                    'Originally purchased for a pretty penny, '.
                                    'unwanted gifts often sell for just pennies '.
                                    'on the dollar. Check our listings below to '.
                                    'see for yourself or enter a couple of '.
                                    'keywords into our quick search and find '.
                                    'the unwanted gift that says, "You shouldn\'t'.
                                    ' have, but I\'m sure glad you did!"',

    'desc_tiny_unwanted'        => 'New items @ used&nbsp;prices!',

    'desc_tabs_unwanted'        => '<h2>Unwanted Gifts Search</h2>
        <p>Why waste time searching through dozens of items to find
        one in the right condition, when you can find an unwanted gift
        in moments?</p>
        <h3>So save time, and money</h3>
        <p>Just Click \'Search Now\', and away you go!</p>',

    'formhelp_unwanted'         => 'Just enter a search term!',

    'formhelp_searchterm'       => 'Enter a search term',

    #localfurniture
    'desc_long_furniture_title'=> 'Crazy Local Furniture Search',
    'desc_long_furniture1'     => '<em>find local furniture at rock bottom prices!</em>',
    'desc_long_furniture2'     => 'Use Local Furniture Search to find tables, chairs, '.
                                    'sofas, and large appliances close to home and '.
                                    'take home big savings on these bulky items.',
    'desc_long_furniture3'     => 'Fluctuating fuel prices make it difficult for '.
                                    'eBay sellers to estimate shipping costs. High '.
                                    'estimates make it even harder for them to sell '.
                                    'large appliances and furniture items. When you '.
                                    'shop using the Crazy Local Furniture Search, '.
                                    'it\'s a win-win for both you and the seller.',
    'desc_long_furniture4'     => 'Crazy Local Searches make it easy to find furniture.'.
                                    ' Simply begin by adding your postcode. Next add the '.
                                    'price you are willing to pay and the distance you are '.
                                    'willing to travel to pick up your furniture bargain.',
    'desc_long_furniture5'     => 'Further refine your search by adding categories and/or '.
                                    'keywords. This unique feature scans all furniture '.
                                    'listings and does all of the hunting for you. We '.
                                    'show you just what you want to see where '.
                                    'you want to see it. <strong>Try it now!</strong>',

    'desc_short_furniture_title'=> 'Local Furniture Search',
    'desc_short_furniture1'     => 'Local furniture on eBay at rockbottom prices!',
    'desc_short_furniture2'     => 'If it\'s chairs, sofas, tables or other large '.
                                    'furniture items you need, use the Local Furniture '.
                                    'Search to help you grab a local bargain. It is a struggle to sell'.
                                    ' even the best furniture on eBay because of the difficulties of '.
                                    'delivery. Let us find great bargains very near '.
                                    'to where you live.',

    'desc_tiny_furniture'       => 'Local furniture at rockbottom prices!',

    'desc_long_joblot_title'   => 'Local Job Lots Search',
    'desc_long_joblot1'        => 'If you are unfamiliar with  job lots, they are generally a '.
                                    'collection of assorted merchandise within a category. '.
                                    'Job lots let you turn inexpensive wholesale purchases into '.
                                    'retail profits. The Local Job-Lots Search helps you'.
                                    ' extend your purchasing power by eliminating freight costs.',
    'desc_long_joblot2'        => 'Everybody loves a sale. Whether you are a store-front '.
                                    'merchant, a flea market entrepreneur, or a dedicated eBay '.
                                    'seller, you can treat your customers to close-out retail '.
                                    'shopping with job lots.',
    'desc_long_joblot2'        => 'Normally a wholesaler\'s "Must Go" items, job lot purchases '.
                                    'give you the chance to mark-up at less than retail, '.
                                    'keeping your sales in the black while still offering '.
                                    'bargain buys to your customers.',
    'desc_long_joblot3'        => 'Often sold in bulk quantities, job lots are a hard sell '.
                                    'for the wholesaler who must pass on shipping costs to '.
                                    'his buyers. The Local Job-Lots Search is '.
                                    'great for finding these bulk bargains in your local area.',
    'desc_long_joblot4'        => 'It\'s easy to get started with local job lot searches'.
                                    '. Just enter your postcode and click. Next, add '.
                                    'your job lot budget and the distance you\'ll travel to '.
                                    'pick up your job lot bargain. Further refine your search '.
                                    'by using keywords and/or categories and sub-categories. '.
                                    'Try it now!',

    'desc_short_joblot_title'   => 'Local Job-Lot Search',
    'desc_short_joblot1'        => 'Bulk items, at low-low prices on eBay!',
    'desc_short_joblot2'        => 'Our Local Job Lots Search is great '.
                                    'for finding underpriced bulk bargains in your local area. Buy '.
                                    'them cheap and maybe sell them for more at car boot sales or '.
                                    'even sell them back on eBay,.'.
                                    'and if you don\'t have to pay courier or '.
                                    'delivery charges for your stock, then so much the better.',

    'desc_tiny_joblot'          => 'Buy Cheap, Sell High!',

    'desc_long_baby_title'      => 'Local Baby Bargain Search',
    'desc_long_baby1'           => '<em>Big Bargains For Baby Prices</em>',
    'desc_long_baby2'           => 'Time and money can be particularly precious when you have '.
                                    'a baby and while baby furnishings and equipment are '.
                                    '"oh so dear" to the heart, they are often "oh so dear" to '.
                                    'the budget as well. Additionally, a great deal of time '.
                                    'spent while expecting a baby is spent in preparing '.
                                    'for baby\'s arrival. Besides the extra cash needed to '.
                                    'pay for delivery of baby gear, delayed deliveries of '.
                                    'baby furnishings and equipment can put you into a panic.',
    'desc_long_baby3'           => 'The Local Baby Bargain Search smooths the way '.
                                    'by helping you keep baby shopping under control.'.
                                    '<ul><li>'.
                                    'Eliminate the wait. Find  baby bargains close to home.</li>'.
                                    '<li>Keep costs down by wiping out shipping charges. '.
                                    'Choose the range you are willing to '.
                                    'travel to pick up your purchase.</li>'.
                                    '<li>Be sure the items you purchase are in the condition '.
                                    'you expected before you take possession.</li>',
    'desc_long_baby4'           => 'We make things easy from start to finish. Simply begin '.
                                    'by adding your postcode to our form. Next add the price '.
                                    'you expect to pay and the distance you can travel to pick '.
                                    'up your merchandise. Refine your search with categories '.
                                    'and/or keywords. Let us help you shop local and '.
                                    'find the best bargain baby essentials. Our Baby '.
                                    'Bargain Search is the perfect tool to help you do that.'.
                                    ' Try it now!',

    'desc_short_baby_title'     => 'Baby Bargain Search',
    'desc_short_baby1'          => 'Big Bargains, like cots or prams, for Baby'.
                                    ' Prices on eBay',
    'desc_short_baby2'          => 'Time and money can be particularly precious '.
                                    'when you have a baby and while baby stuff is '.
                                    '"oh so dear"  to the heart, it is also often '.
                                    '"oh so dear" to the budget as well! Let us '.
                                    'help you find the best bargain baby essentials. '.
                                    'Shop local and avoid expensive delivery costs. '.
                                    'Let us help you do that!',
    'desc_tiny_baby'            => 'Big Bargains For Baby Prices',

    'desc_long_laptop_title'    => 'Local Laptop Search',
    'desc_long_laptop1'         => 'The safer way to buy a laptop',
    'desc_long_laptop2'         => 'Your good buy on a laptop or netbook can become an '.
                                    'expensive investment when someone along the road '.
                                    'doesn\'t "handle with care."',
    'desc_long_laptop3'         => 'Say "good-bye" to the chance of a damaged delivery.'.
                                    ' Our local laptop search puts you on the '.
                                    'fast track to safe transit as well as cash savings.'.
                                    ' Our search lets you pick up the best laptop and '.
                                    'netbook bargains on eBay, just minutes '.
                                    'away from your home.',
    'desc_long_laptop4'         => 'It\'s easy to get started with our local laptop '.
                                    'search. First, enter your ZIP Code. Then use our '.
                                    'search feature to enter the distance you\'ll '.
                                    'travel to pick up your purchase and the amount '.
                                    'you have budgeted to buy it.',
    'desc_long_laptop5'         => 'Our unique local laptop search looks through all '.
                                    'the laptop and netbook listings to find those '.
                                    'in your ranges of both price and location. '.
                                    'Refine your search by adding categories, '.
                                    'subcategories, and/or keywords. Then scroll '.
                                    'through the listings to find your best buys in '.
                                    'used, refurbished, and new computers. All of '.
                                    'them just minutes away with not a penny '.
                                    'wasted on postage. Try it now!',

    'desc_short_laptop_title'   => 'Local Laptop Search',
    'desc_short_laptop1'        => 'Take out the risk of buying Laptops '.
                                    'and Netbooks on eBay',
    'desc_short_laptop2'        => 'Buy local and meet the seller in person. '.
                                    'Our Local Laptop Search tool will help you to meet '.
                                    'laptop sellers face to face - and not a penny wasted on Postage.',

    'desc_tiny_laptop'          => 'The Safer Way To Buy A Laptop',

    # Carparts
    'desc_long_carparts_title'  => 'Local Auto Parts Search',
    'desc_long_carparts1'       => '<em>Vintage car parts - Trendy accessories</em>',
    'desc_long_carparts2'       => 'That car part or auto accessory may not be as '.
                                    'difficult to find as you thought. In fact, it '.
                                    'might be just around the corner! Use the '.
                                    'local auto parts search to speedily '.
                                    'put your vehicle back into shape.',
    'desc_long_carparts3'       => 'No wait to get rolling. No delivery charges. '.
                                    'Just a short trip to claim your purchase '.
                                    'saves you both time and money.',
    'desc_long_carparts4'       => 'Just add your ZIP Code to our search form and '.
                                    'away you go! Put in a price and the distance '.
                                    'you are willing to travel to pick up your car '.
                                    'part. Refine your search with a keyword '.
                                    'and/or categories. After you\'ve found the '.
                                    'car part that you would like to buy, we even '.
                                    'add a map to make it easy to find your '.
                                    'seller! Try it now!',

    'desc_short_carparts_title' => 'Local Carparts Search',

    'desc_short_carparts1'      => 'Great parts, on eBay at rock bottom '.
                                    'prices, often just around the corner!',
    'desc_short_carparts2'      => 'Our local Car Parts search will help you to find car parts, '.
                                    'go a little further out of your way and'.
                                    ' you can grab some real bargains, such as a set of alloys for '.
                                    'under &pound;50!',

    'desc_tiny_carparts'        => 'Parts at rockbottom prices!',

    # Cars
    'desc_long_cars_title'  => 'Local Car Search',
    'desc_long_cars1'       => '<em>Find cars at great prices in your local area</em>',
    'desc_long_cars2'       => 'Try eBay for your next car purchase. '.
                                'eBay is often overlooked as a place to buy a car, but thousands of cars '.
                                'sell every day on eBay, and we can quickly show you '.
                                    'the best deals in your local area.',
    'desc_long_cars3'       => 'Give our quick search page a try. Just enter your postcode. '.
                                    'Later you can refine your search.',

    'desc_short_cars_title' => 'Local Car Search',


    'desc_tabs_cars'       => '<h2>Local Cars at Great Prices</h2>
        <p>Find your next car, near by, at a great price!</p>
        <h3>Save Time, Buy Local</h3>
        <p>Just enter your postcode and search now</p>',
    'desc_short_cars1'      => 'Find your next car, near by, at a great price!',

    'desc_tiny_cars'        => 'Cars at rockbottom prices!',

    # Buy It Now
    'desc_long_bin_title'  => 'Buy It Now Search',
    'desc_long_bin1'       => '<em>Find listed items available for immediate sale.</em>',
    'desc_long_bin2'       => 'Try eBay for your next fixed price purchase. '.
                                'Although eBay is often remembered for '.
                                    'it\'s auction style listings, it is becoming'.
                                'ever more popular as a source of fixed price retail items.'.
                                'We can help you to find all sorts of bargains '.
                                    'available for you to buy immediately '.
                                'at a fixed price.',
    'desc_long_bin3'       => 'We apply special seller filters so that you are only '.
                                    'presented with listings where the seller '.
                                'has an established good reputation with eBay and a '.
                                    'no quibble returns policy.',
    'desc_long_bin4'       => 'Give our Buy It Now quick search page a try. '.
                                    'Later you can refine your search.',

    'desc_short_bin_title' => 'Buy It Now Search',

    'desc_short_bin1'      => 'Find your next bargain, from a reputable seller, at a best fixed price!',

    'desc_tiny_bin'        => 'Bargains at good fixed prices!',

    'desc_long_amazon_title'   => 'Special Feature: Search Amazon.com',
    'desc_long_amazon1'        => 'This search is temporarily out of service.',
    'desc_long_amazon2'        => 'Probably one of the biggest department stores on the web and '.
                                    'maybe world-wide, Amazon.com has some of just about everything '.
                                    'and from just about anybody: from online merchants to your '.
                                    'next door neighbor.',
    'desc_long_amazon3'        => 'If you are wondering if the auction item that caught your eye is '.
                                    'really a bargain, search Amazon.com and see. You might just find '.
                                    'it for even less money than you intended to spend.',
    'desc_long_amazon4'        => 'Amazon.com puts you in touch with over 2-million sellers world-wide '.
                                    'and we can puts Amazon.com bargains at your fingertips. We\'ve cut'.
                                    ' the confusion with our customized search feature. Just type in a '.
                                    'keyword or two to begin and our Amazon Search lists the available '.
                                    'items. Click on one and go straight to Amazon.com for all the details'.
                                    ' or to complete your purchase.',
    'desc_long_amazon5'        => 'Let us streamline web shopping for bargain hunters. Our customized '.
                                    'searches put you just a click away from the best buys on the web. '.
                                    'Try it now! Search Amazon.com.',

    'desc_short_amazon_title'   => 'Amazon Search',
    'desc_short_amazon1'        => 'Search Amazon for bargains',
    'desc_short_amazon2'        => 'just type a keyword and search!',
    'desc_tiny_amazon'          => 'just type a keyword and search!',

    'desc_short_buynow_title'   => 'BuyNow Search',
    'desc_short_buynow1'        => 'Search for stuff to BuyNow',
    'desc_short_buynow2'        => 'just type a keyword and search!',

    'desc_tiny_buynow'          => 'just type a keyword and search!',

    'desc_tabs_buynow'          => '<h2>Buyit Now Search</h2>
        <p>Why waste time waiting for an auction to end?
        Search Now with our Buy it Now search</p>
        <h3>Save time, Save money, Buy it Now!</h3>
        <p>Just enter your search term and click Search Now!</p>',

    'desc_long_buynow_title'   => 'Special Feature: Find stuff to Buy Now at fixed prices!',
    'desc_long_buynow1'        => 'Here is the website where bargain hunters can always find the'.
                                    ' best buys. These days, bargain hunters must shop and compare '.
                                    'prices to catch the really great deals. That\'s one great '.
                                    'reason we offer our special bargain hunting feature: '.
                                    'Find items to Buy Now.',
    'desc_long_buynow3'        => 'If you are wondering if the auction item that caught your eye is '.
                                    'really a bargain, search for \'Buy It Now\' items and see. You might just find '.
                                    'it for even less money than you intended to spend.',
    'desc_long_buynow4'        => 'Putting you in touch with millions of sellers world-wide '.
                                    'and we put these bargains at your fingertips. We\'ve cut'.
                                    ' the confusion with our customized search feature. Just type in a '.
                                    'keyword or two to begin and our \'Buy It Now\' Search lists the available '.
                                    'items. Click on one and go straight to eBay for all the details'.
                                    ' or to complete your purchase.',
    'desc_long_buynow5'        => 'We streamline web shopping for bargain hunters. Our customized '.
                                    'searches put you just a click away from the best buys on the web. '.
                                    'Try it now! Search \'Buy It Now\' items now.',

    'desc_short_buynow_title'   => 'Buy It Now Search',
    'desc_short_buynow1'        => 'Search eBay for fixed price bargains',
    'desc_short_buynow2'        => 'just type a keyword and search!',

    'desc_tiny_buynow'          => 'just type a keyword and search!',

    # Vintage search
    'desc_short_vintage_title'  => 'Vintage Search',
    'desc_short_vintage1'       => 'Find vintage items at great prices!',

    # Contact Page

    'contact_thanks'        => 'Thank you for contacting BayCrazy.com',
    'contact_acknowledge'   => 'Your message has now been sent to the support team!',
    'contact_nomessage'     => 'Sorry, you haven\'t included a message. '.
                                'Please try again.',
    'contact_noemail'       => 'You didn\'t include your email address: We '.
                                'will receive your message but we won\'t be able to reply to you.',
    'contact_close'         => 'Close This Form',
    'contact_title2'         => 'We are always keen to hear your feedback, '.
                                'suggestions, criticism or even praise.',
    'contact_faqtitle'      => 'But first, if you could read these answers to '.
                                'common questions it should save us all some time!',
    'contact_qsell'         => 'Q: How do I sell on here?',
    'contact_asell'         => 'A: You don\'t! We provide a searches to help '.
                                'buyers find underpriced bargains which are listed on eBay. We '.
                                'don\'t directly accept listings.',
    'contact_qjoin'         => 'Q: How do I join?',
    'contact_ajoin'         => 'A: You don\'t! There is no need to \'join this website\' to use it!<br />'.
                                'You might want to sign up with eBay, which can be '.
                                'done at <a href=\'https://scgi.ebay.co.uk/ws/eBayISAPI.dll?'.
                                'RegisterEnterInfo\' target="_blank">www.ebay.co.uk</a> <br />'.
                                'We welcome you to visit us on '.
                                '<a href =\'http://www.facebook.com/pages/BayCrazycom/82807396602\' target="_blank">'.
                                'Facebook</a>, but that is optional and free.',
    'contact_qbid'          => 'Q: I have bid more than I meant to, can '.
                                'you cancel the bid?',
    'contact_abid'          => 'A: You need to use eBay\'s procedure as '.
                                'described <a href=\'http://pages.ebay.co.uk/help/buy/questions/'.
                                'retract-bid.html\' target=\'_blank\'>HERE.</a> Do it immediately.',
    'contact_qcontact'      =>  'If your question isn\'t listed click here to contact us',
    'contact_text1'         => 'All fields on this form are optional and any '.
                                'information you choose to give us will be used in strict accordance '.
                                'with our <a href="/content/privacy" '.
                                'target="Privacy">Privacy Policy</a>',
    'contact_text2'         => 'We will only use your contact details if you '.
                                'select \'Please contact me\'',
    'contact_name'          => 'Your name:',
    'contact_email'         => 'Your email address:',
    'contact_subject'       => 'Subject:',
    'contact_contactme'     => 'Please contact me:',
    'contact_message'       => 'Message:',
    'contact_send'          => 'Send Message',

    # ebay403

    '403_title'         => 'Sorry!!!',
    '403_text1'         => 'We are sorry, but we cannot process your page '.
                            'request at this time.',
    '403_text2'         => 'Your browser reports that you have just come to '.
                            'us directly from ebay.',
    '403_text3'         => 'ebay have strict and rather complex rules on this '.
                            'matter and they do not permit us to accept your page request '.
                            'at this time.',
    '403_text4'         => 'To preserve our commercial relationship with ebay, '.
                            'we must give you this rather crummy error message. Sorry. ',
    '403_text5'         => 'It\'s not actually an error, but is the result of '.
                            'us trying our best to maintain this site in compliance '.
                            'with ebay\'s rules.',
    '403_text6'         => 'We apologise sincerely, and we will continue to '.
                            'try to improve this process to give you the best '.
                            'user experience possible.',
    '403_text7'         => 'Meanwhile, you might be able to avoid this message '.
                            'if you ensure you open items in new tabs, or a new window '.
                            '<a href="http://www.baycrazy.com">www.baycrazy.com</a>',
    '403_text8'         => 'If you could <a href="/content/contact/">contact us</a> '.
                            'and do your best to explain how this error happened, '.
                            'it may help us to prevent it in the future.',

    # notfound
    'notfound_title'    => 'Sorry, the page could not be found!',
    'notfound_text1'    => 'Maybe you followed a bad link to our site, or perhaps '.
                            'we\'ve got gremlins throwing a spanner in the works',

    # manchester


    'manchester_title'  => 'Find absurdly low priced bargains in '.
                            'and around Manchester! ',
    'manchester_text1'  => 'With Our Manchester local search we can '.
                            'help you find <b>underpriced bargains</b> in your '.
                            '<b>local pick-up</b> area.',
    'manchester_text2'  => 'Our special search tool will show you eBay '.
                            'listings within your neighbourhood, where prices are unusually low '.
                            'because the seller does not want to post the item and would prefer '.
                            'you to collect it in person. By restricting himself to \'collection '.
                            'only\' he has seriously reduced the number of likely bidders. That '.
                            'in turn will almost guarantee low purchase prices. Maybe it\'s a new '.
                            'seller and he simply doesn\'t understand how eBay works. Or maybe they '.
                            'just want\'s the item out of their way. In any case, you are sure to '.
                            'find lots of underpriced bargains by using our site',
    'manchester_text3'  => 'The very best bargains to be found are <b>large '.
                            'or heavy items</b> such as TVs, Furniture, Computers, Baby Items, '.
                            'Fitness equipment, Alloy wheels etc. All you need to do is bid, win, '.
                            'and pop around to the sellers house to pick up a bargain at what '.
                            'may be an absurdly low price. The seller may sulk as you hand him  '.
                            'just a few pounds or even 99p for that item - but what the heck, at '.
                            'least he found a buyer! Tell him about BayCrazy.com and that might '.
                            'cheer him up.',
    'manchester_text4'  => 'Just click your approximate location on the map '.
                            'and we\'ll do the rest!',
    'manchester_text5'  => 'Alternatively, you can just enter your post-code '.
                            'and click the \'Quick Search\' button. Later, you can refine your '.
                            'search further with maximum distance or maximum bid. You can even '.
                            'restrict yourself to one ebay category and bookmark the search to '.
                            'pop back later.',
    'manchester_text6'  => 'This is a completely free service: There\'s no '.
                            'need to enrol or subscribe: Check out our other free bargain search '.
                            'facilities at <a href=\'http://www.baycrazy.com\'>www.baycrazy.com',

    # search pages titles
    # pageTitle

    'misspelled_title'  => 'Misspelt Title Bargains',
    'nightowl_title'    => 'Night Time Bargains',
    'unwanted_title'    => 'Unwanted Gifts',
    'rightnow_title'    => 'Night Time Bargains',
    'joblot_title'      => 'Job-Lot Bargains',
    'local_title'       => 'Local Bargains',
    'carpart_title'     => 'Local Car Parts',
    'contact_title'     => 'Contact Us Page',
    'faq_title'         => 'Frequently Asked Questions',
    'sell_title'        => 'Selling on eBay',

    # filtermenus

    'filter_try'        => 'Try it now!',
    'filter_pc'         => 'Enter postcode',
    'filter_pc_us'      => 'Enter zip code',
    'filter_pc_uk'      => 'Enter postcode',
    'filter_pc_gb'      => 'Enter postcode',
    'filter_pc_au'      => 'Enter postal code',
    'filter_pc_ca'      => 'Enter postal code',
    'filter_pc_de'      => 'Geben Sie Ihre Postleitzahl',
    'filter_pc_de_num'  => 'Postleitzahlen dÃ¼rfen nur Ziffern enthalten, keine Buchstaben '.
                            'oder Leerzeichen (Keine Zeichensetzung)',
    'filter_pc_us_num'  => 'You are using the USA version of the site '.
                            ' where Zip Code must be numeric characters only: '.
                            '(No spaces or commas)\nIf you are not in the USA, '.
                            'then click the correct country flag and try again.',
    'filter_pc_au_num'  => 'You are using the USA version of the site '.
                            ' where Zip Code must be numeric characters only: '.
                            '(No spaces or commas)\nIf you are not in the USA, '.
                            'then click the correct country flag and try again.',
    'filter_pc_uk_num'  => 'Postcodes may contain only letters and numbers, '.
                            'digits and spaces (No punctuation)',
    'filter_pc_ca_num'  => 'Postal codes may contain only letters, '.
                            'digits and spaces (No punctuation)',
    'filter_noinput'    => 'Please enter the correctly spelt '.
                            'search expression',
    'filter_pc_max'     => 'You must enter a valid full British postcode.\n',
    'filter_max'        => 'Max length for the search expression is '.
                            '30 characters',
    'filter_postcode_fixed' => ' isn\'t a valid postcode, so we\'ve performed the search using ',
    'filter_postcode_invalid' => ' isn\'t a valid postcode, check to make sure you haven\'t mistyped it '.
                                 'for exaple, written a letter \'O\' where a zero should be, or vise versa',
    'filter_min'        => 'Min length for the search expression is '.
                            '3 characters',
    'filter_invalid'    => 'Search expressions may contain only '.
                            'letters and numbers and spaces (No punctuation)',
    'filter_lc_header'  => 'Search out items which the seller doesn\'t want to post, '.
                            'but which are on your doorstep. <br />'.
                            'That makes for some Crazy Bargains for you to collect.',
    'filter_lc_title'   => 'Local Bargain Search',
    'filter_lc_text1'   => ' Search out items which the seller doesn\'t want to '.
                            'post, but which are on your doorstep. ',
    'filter_lc_text2'  => 'That makes for some Crazy Bargains for you to collect.',
    'filter_cars_title'   => 'Local Car Search',
    'filter_cars_text1'   => 'eBay is now a great place to buy a car '.
                            'and we can help you find bargains which are in your local area. ',
    'filter_cars_text2'  => 'Our search is really easy. Set your price, your postcode, and your idea of local',
    'filter_search'    => 'Search Now',
    'filter_refine'    => 'Refine Your Search',
    'filter_local'     => 'What\'s your idea of local?',

    'another_category'  => 'Advanced Option: Select an additional category',

    'filter_uw_title'  => 'Unwanted Gift Search',
    'filter_uw_header' => 'Seek out items which the seller, quite simply doesn\'t want to own.<br>'.
                            'He may not know the item\'s value and he may not care.<br>'.
                            'Their junk might be your treasure.',
    'filter_uw_text1'  => 'Seek out items which the seller, quite simply '.
                           'doesn\'t want to own.<br/>He may not know the item\'s '.
                           'value and he may not care.',
    'filter_uw_text2'  => 'Their junk might be your treasure.',

    'filter_bin_title'  => 'Buy It Now Search',
    'filter_bin_text1'  => 'Seek out fixed price items from reputable sellers. ',
    'filter_bin_text2'  => 'We select sellers based on their feedback, status and reputation with eBay. '.
                            'So you can buy with confidence.',
    'filter_bin_text3'  => 'Just enter a search term and set your price budget.',
    'filter_bin_text4'  => 'Enter up to 2 keywords.A brand name is ideal!',

    'filter_ms_fb'     => '',

    'filter_ms_title'  => 'Misspelt Title Search',
    'filter_ms_header' => '<p>Some careless sellers mistype critical words in the listing title.<br />'.
                          'That makes them harder for other bidders to find.</p>'.
                          '<h3>Less bidders = Lower prices</h3>'.
                          '<p>Enter up to 2 keywords.<br>'.
                          'One word is best<br>'.
                          'especially a brand name!<br>'.
                          '<span style=\'color:red\'><b>You MUST spell it CORRECTLY :o)</b></span><br>'.
                          'Enter up to 2 keywords.<br/>A brand name is ideal!</p>'.
                          '<h3>Refine Your Search</h3>',
    'filter_ms_text1'  => 'Some careless sellers mistype critical words in the listing title.',
    'filter_ms_text2'  => 'That makes them harder for other bidders to find.',
    'filter_ms_text3'  => 'Less bidders = Lower prices',
    'filter_ms_text4'  => 'Enter up to 2 keywords.<br/>One word is best<br/>especially a brand name!',
    'filter_ms_text5'  => 'You MUST spell it CORRECTLY :o)',
    'filter_ms_text6'  => 'Enter up to 2 keywords.<br/>A brand name is ideal!',

    'filter_am_title'  => 'Amazon Search',
    'filter_am_text1'  => 'Search Amazon for bargains, ',
    'filter_am_text2'  => 'just type a keyword and search!',

    'filter_no_title'  => 'Night Time Bargain Search',
    'filter_no_text1'  => 'Some careless sellers have their listings end in the dead of night.',
    'filter_no_text2'  => 'That makes them less open to last minute bidders.',
    'filter_no_text3'  => 'Less bidders = Lower prices.',
    'filter_no_text4'  => 'When Should \'Crazy Time\' Begin?',
    'filter_no_text5'  => 'When Should \'Crazy Time\' End?',

    'filter_rn_title'  => 'Ending Now : 0 Bids',
    'filter_rn_header' => '<p>Just look what\'s finishing <b>\'Right Now.\'</b><br />These listings '.
                            'end within the next few minutes: But no-one has yet bid on them!<br />'.
                            'Grab these bargains while you can!<br/>It\'s you against the clock!</p>',
    'filter_rn_text1'  => 'Just look what\'s finishing <b>\'Right Now.\'</b>',
    'filter_rn_text2'  => 'These listings end within the next few minutes: But no-one has yet bid on them!<br>',
    'filter_rn_text3'  => 'Grab these bargains while you can!<br>It\'s you against the clock!',
    'filter_rn_text4'  => 'Less bidders = Lower prices.',

    'filter_minprice'  => 'Minimum price?',
    'filter_maxprice'  => 'The most you want to spend?',
    'filter_price2'    => 'Max price?',

    'filter_buynow_title' => 'Items to Buy Now',
    'filter_buynow_header' => '<p>Seek out fixed price items from reputable sellers. <br>'.
                                'We select sellers based on their feedback, status and reputation with eBay. '.
                            'So you can buy with confidence.</p>'.
                                '<h3>Just enter a search term and set your price budget.</h3>',
    'filter_buynow_text1'  => 'Seek out fixed price items from reputable sellers.',
    'filter_buynow_text2'  => 'We select sellers based on their feedback, status and reputation with eBay. '.
                            'So you can buy with confidence.',

    'filter_searchword'=> 'Enter a keyword (Optional)',

    'filter_fullsite'   => 'For more features, try the full site: ',

    'footer_text1'     => 'BayCrazy is an independent search site and not a part of eBay',
    'footer_text2'     => 'Privacy Policy',
    'footer_text3'     => 'About Us',
    'footer_text4'     => 'Mobile Site',

    'cookies'           => 'Baycrazy.com uses cookies. Some may have been set '.
                            'already. Please click the button to accept our '.
                            'cookies. If you continue to use the site, we\'ll '.
                            'assume you\'re happy to accept the cookies anyaway.',

    'skip_nav'         => 'Skip to navigation (Press Enter)',
    'skip_content'     => 'Skip to main content (Press Enter)',

    'draw_local'       => 'Local Search Criteria',
    'all_cats'         => 'All Categories',
    'all_subcats'      => 'All Subcategories',

    # tabuluate

    'tab_price'        => 'Price',
    'tab_shipping'     => 'Shipping',
    'tab_end'          => 'End Time',
    'tab_left'         => 'Time Left',
    'tab_perpage'      => 'Items per page',
    'tab_pic'          => 'Picture Size',
    'tab_distance'     => 'Distance',

    'tab_error'        => 'OOPS! We suffered an error and ebay '.
                           'returned no items.',
    'tab_error1'       => 'This is a temporary glitch with ebay\'s search '.
                           'facility. Ebay\'s engineers have been alerted and we apologise '.
                           'for any inconvenience.',
    'tab_error2'       => 'Please refresh this page in a few minutes time.',
    'tab_error115a'    => 'According to eBay, the postcode you entered was not '.
                           'recognised for the country you searched.',
    'tab_error115b'    => 'Most likely that is because you did not enter a '.
                           'full postcode, or you used the letter \'o\' or capital \'O\' where '.
                           'you should have used the number \'0\', or maybe the other way around?',
    'tab_error115c'    => 'Or maybe you have entered the correct postcode, but '.
                           'we are searching the wrong country site? Check that the right flag '.
                           'is shown enlarged and to the right of the row of '.
                           'flags just above this message.',
    'tab_error115d'    => 'You entered the postcode as',

    'switch_country'    => 'It looks like your postal code is from another '.
                            'country. Do you want to switch to ',
    'must_set'          => 'You must set',

    # api error

    'api_error_guess1' => 'eBay seems to have a problem with the postcode you\'ve provided (',
    'api_error_guess2' => '), so we\'ve had a guess and used this one:',
    'api_error_guess3' => 'To get more accurate results, you might want to try re-entering your postcode',
    'api_error_guess4' => 'If you\'re sure you entered your postcode correctly, please ',
    'api_error_guess5' => 'Status',
    'api_error_guess6' => ' so could you please try entering your postal code again?',

    # Miscellaneous phrases
    'find_us_on_facebook' => 'Find us on Facebook',
    'day'                 => 'Day',
    'hour'                => 'Hour',
    'min'                 => 'Min',
    'sec'                 => 'Sec',
    'filter_select_cat'   => 'Select Category',
    'time_unit_plural'    => 's',
    'bid_plural'          => 's',
    'bid'                 => 'bid',
    'local_pickup'        => 'Check Listing',
    'filter_select_subcat'=> 'Select Sub-category (Optional)',
    'add_a_comment'       => 'Leave a comment',

   'api_error'           => 'OOPS! We\'ve suffered a error<p>Our engineers have been informed. '.
                            'Please try starting your search again from the home page.',
   'default_postcode_warning' =>'Important: You are looking at items near to the postcode SW1A 1AA.'.
                            'Please search again after entering your own correct postcode.',

   'map_to'             => 'Map to',

   'country_selection'  => 'Country Selection',
    'settings'          => 'Settings',

    'desc_long_switchcountry1'    => '<ul id="switchcountry">
                    <li class="uk"><a href="/local/index/region/UK/">UK</a></li>
                    <li class="us"><a href="/local/index/region/US/">US</a></li>
                    <li class="au"><a href="/local/index/region/AU/">Australia</a></li>
                    <li class="de"><a href="/local/index/region/DE/">Deutschland</a></li>
                    <li id="last" class="ca"><a href="/local/index/region/CA/">Canada</a></li>
              </ul>',

    'no_results'    => 'Sorry, there are no results to be shown.',

    'no_results_long'   => '<h2>Sorry, it doesn\'t seem there '
                            . 'are any results to show.</h2>'
                            . '<p>A few things you <strong>should check</strong>, to be '
                            . '<em>sure</em> there is really nothing to see here</p>'
                            . '<ul>'
                            . '<li>Is the postal code correct?</li>'
                            . '<li>Is there a keyword entered that shouldn\'t be?</li>'
                            . '<li>Is the search area too small?</li>'
                            . '<li>Is the max price too low?</li>'
                            . '<li>Is there a category set, that perhaps shouldn\'t '
                            . 'be there, or should be something else?</li>'
                            . '</ul>',
    
    'desc_short_superweekend1' => 'WOW! THESE DEALS ARE ON NOW! <img src=" http://ir.ebaystatic.com/'.
                                    'pictures/aw/uk/holiday/Doodle2014/SuWe_August_UK/'.
                                    '4043_UK_Retail_AugustWOW_Doodle_Small_150x30_C02.png">',
    
    'desc_long_superweekend' => '
    <h1>WOW! THESE DEALS ARE NOW: <a href="/outlink/redirect/page/superweekend/" target="_blank">
                    <img src=" http://ir.ebaystatic.com/'.
                    'pictures/aw/uk/holiday/Doodle2014/SuWe_August_UK/'.
                    '4043_UK_Retail_AugustWOW_Doodle_Small_150x30_C02.png"></a></h1>

<h2>eBay Have Selected Some Of The UK\'s Best Deals and Made Them Better</h1>

<p>All Brand New, <strong>Branded Goods</strong> And Priced To Go <span class="deals">Up To 60% Off RRP!</span></p>

<h2 class="red">WOW!</h2>

<p>Watches, iPads, iPhones, Consoles, Fashion and So Much More!</p>

 <ul>
 <li>Recommended Retail Prices BASHED!</li>
 <li>Amazon Prices SMASHED!</li>
 <li>Even eBay\'s Previous Best Prices CRUSHED!</li>
 </ul>
 <a href="/outlink/redirect/page/superweekend/" target="_blank">
<img src="http://pagead2.googlesyndication.com/pagead/imgad?id=CICAgKCTiMXeXhABGAEyCFfZb9u9Anhu">
</a>
 
 <h2>Some of these are probably the cheapest on the internet!</h2>

<h3>All First Class Branded Goods From Specially Selected Sellers!</h3>

<h3>All For A Limited Time Only, With Limited Stock! Buy Them Now!</h3>

<p>These Offers Expire on Thursday (4th September), so snap them up quick!</p>',



    'not_found'     => 'Sorry, the page \'%contentid%\' does not exist',
    'go_back'       => 'Please go back to our <a href="/">home page</a> and our searches',
);
