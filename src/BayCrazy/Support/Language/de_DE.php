<?php

return array (

    #imagetags

    'tag_bc_logo'           => 'bayCrazyLogoDe.png',
    'tag_search_button_img' => 'searchbutton.png',

    'header_title'          => 'Finde verborgene Schnäppchen auf eBay',

    #searchstrings

    'arg_local'             => '("Abholung","abholen","Selbstabholung","Abhol", '.
                                '"Selbstabholer","Kein Versand","Abholen")',

    'arg_unwanted'          => '("Ungewolltes Mitbringsel", "Unpassendes Geschenk", "Ungewolltes Geschenk", '.
                               '"Ungewolltes Präsent", "Weihnachtspräsent", "Geburtstagspräsent", '.
                               '"Weihnachtsgeschenk", "Geburtstagsgeschenk")',

    #meta keywords

    'kwd_local'             => 'Schnäppchen Auktionen, eBay lokal Schnäppchen, eBay Suche,  '.
                               'eBay lokale Schnäppchensuche, eBay Schreibfehler, '.
                               'Schnäppchensuche lokal, Schreibfehler Suche, '.
                               'Schreibfehler Auktionensuche',

    'kwd_cars'             => 'auction bargains, ebay local car bargains, ebay car search, '.
                               'ebay local car finder, ebay misspelt, '.

                               'local bargain search, misspelling search, '.
                               'auction misseplling search',

    'kwd_unwanted'          => 'Ungewolltes Geschenk, Ungewolltes Präsent, Ungewollter Geburtstag, '.
                               'Ungewolltes Weihnachten, Ungewolltes Xmas, Ungewollter Gutschein, '.
                               'Ungewolltes Mitbringsel, Ungewolltes Präsent, Unpassendes Geschenk, '.
                               'Schnäppchen Auktionen, eBay lokal Schnäppchen, eBay Suche, '.
                               'eBay lokale Schnäppchensuche, eBay Schreibfehler, '.
                               'Schnäppchensuche lokal, Schreibfehler Suche, '.
                               'Schreibfehler Auktionensuche',

    #nav
    'nav_home'              => 'Startseite',
    'nav_local'             => 'Lokale',
    'nav_misspelled'        => 'Schreibfehlern',
    'nav_nightowl'          => 'Nacht Schnäppchen',
    'nav_rightnow'          => 'Endet sofort',
    'nav_manchester'        => 'Schnäppchen Berlin',
    'nav_unwanted'          => 'Ungewollte Geschenke',
    'nav_forum'             => 'Mitgliederbereich',
    'nav_furniture'         => 'Möbel',
    'nav_joblot'            => 'Masse Schnäppchen',
    'nav_baby'              => 'Baby',
    'nav_carparts'        => 'Autoteile',
    'nav_cars'            => 'Autos',
    'nav_laptop'          => 'Laptop',
    'nav_contact_us'        => 'Kontakt',
    'nav_help'              => 'Hilfe',
    'nav_faq'               => 'Hilfe',
    'nav_amazon'            => 'Amazon',
    'nav_buynow'            => 'Buy It Now',
    'nav_add_favourites'    => 'Zu Favoriten hinzufügen',
    'nav_on_fb'             => 'auf Facebook',
    'nav_contact'           => 'Kontakt',
    'nav_sell'              => 'Sell',
    'nav_bin'               => 'Buy It Now',
    'nav_moresearches'      => 'Mehr Suchergerbnisse',
    'nav_sitemap'           => 'Sitemap',
    'nav_blog'              => 'Blog',

    #mobile nav
    'm_nav_home'            => 'Startseite',
    'm_nav_local'           => 'Lokale',
    'm_nav_misspelled'      => 'Schreibfehlern',
    'm_nav_unwanted'        => 'Ungewollte',
    'm_nav_nightowl'        => 'Mitternachts Specials',
    'm_nav_bin'             => 'Buy It Now',
    'm_nav_buynow'          => 'Buy It Now',

    #titles
    'title_about'           => 'Über Uns',
    'title_index'           => 'lokale Schnäppchensuche',
    'title_home'            => 'lokale Schnäppchensuche',
    'title_faq'             => 'Häufig gestellte Fragen',
    'title_manchester'      => 'Günstige Angebote rund um Berlin',
    'title_local'           => 'lokale Schnäppchensuche',
    'title_misspelled'      => 'Falsche Schlüsselwörter Schnäppchensuche',
    'title_rightnow'        => 'Auktionen die jetzt enden mit Null Geboten',
    'title_nightowl'        => 'Späte Auktionsendzeiten Suche',
    'title_unwanted'        => 'Ungewollte Geschenke bei eBay',
    'title_furniture'       => 'Günstige Möbel in Ihrer Stadt',
    'title_joblot'          => 'Restposten zum Selbstabholen in Ihrer Stadt',
    'title_baby'            => 'Baby Schnäppchensuche',
    'title_laptop'          => 'Lokale Laptop Schnäppchen',
    'title_carparts'        => 'Lokale Auto-Ersatzteiles Schnäppchen',
    'title_cars'            => 'Lokale Auto Schnäppchen',
    'title_buynow'          => 'Buy Sofort-Kaufen Suche',
    'title_contact'         => '`Kontaktieren Sie uns`',
    'title_privacy'         => 'Datenschutzerklärung',
    'title_amazon'          => 'Amazon Suchseite',
    'title_moresearches'    => 'Alle Suchergerbnisse auf einer Seite!',
    'title_map'             => 'Wegbeschreibungs Service',
    'title_altlocal'        => 'lokale Schnäppchensuche',
    'title_altfurniture'    => 'Günstige Möbel in Ihrer Stadt',
    'title_sell'            => 'Sell your item on eBay',
    'title_bin'             => 'Buy It Now From Selected Sellers With Proven Reliability and Solid Returns Policy',

    #bookmark texts
    'bookmark_about'        => 'Über Uns',
    'bookmark_index'        => 'lokale Schnäppchensuche',
    'bookmark_home'         => 'lokale Schnäppchensuche',
    'bookmark_faq'          => 'Häufig gestellte Fragen',
    'bookmark_home'         => 'Local Bargain Search',
    'bookmark_faq'          => 'Frequently Asked Questions',
    'bookmark_manchester'   => 'Schnäppchensuche Berlin',
    'bookmark_local'        => 'lokale Schnäppchensuche',
    'bookmark_misspelled'   => 'Schreibfehler Schnäppchensuche',
    'bookmark_rightnow'     => '"Endet jetzt" Schnäppchensuche',
    'bookmark_nightowl'     => 'späte Auktionsendzeiten',
    'bookmark_unwanted'     => 'ungewollte Geschenke Suche',
    'bookmark_furniture'    => 'Möbel Schnäppchensuche',
    'bookmark_joblot'       => 'Restposten Schnäppchensuche',
    'bookmark_baby'         => 'Baby Schnäppchensuche',
    'bookmark_laptop'       => 'Laptop Schnäppchensuche',
    'bookmark_carparts'     => 'Lokale Auto-Ersatzteiles Suche',
    'bookmark_cars'         => 'Lokale Autos zu großartigen Preisen',
    'bookmark_contact'      => 'Kontaktieren Sie uns',
    'bookmark_privacy'      => 'Datenschutzerklärung',
    'bookmark_amazon'       => 'Amazon Suche',
    'bookmark_moresearches' => 'mehr Suchparameter',
    'title_map'             => 'Wegbeschreibungs Service',
    'bookmark_altlocal'     => 'lokale Schnäppchensuche',
    'bookmark_altfurniture' => 'Möbel Schnäppchensuche',
    'bookmark_amazon'       => 'Amazon Search',
    'bookmark_buynow'       => 'BuyNow Search',
    'bookmark_moresearches' => 'More Searches',
    'bookmark_map'          => 'Directions Service',
    'bookmark_switchcountry'=> 'Switch Country',
    'bookmark_newsletter'   => 'Newsletter',
    'bookmark_settings'     => 'Settings Manager',
    'bookmark_sell'         => 'Start Selling on eBay',
    'bookmark_bin'          => 'Buy It Now From Selected Sellers',
    #write_content
    'write_content_h2'      => 'Unbedacht eingestellte Schnäppchen bei eBay finden! ',

    'write_content_p'       => 'gräbt einige \'Schätze\' aus '.
                                'die tief in eBay begraben liegen - Wahnsinns Schnäppchen '.
                                'die der Verkäufer falsch eingestellt hat '.
                                'und somit die Artikel quasi zum Fenster raus wirft ',

    #try search functions
    'try_nightowl'          => 'Jetzt die \'Späte Auktionsendzeitsuche\' probieren!',
    'try_furniture'         => 'Jetzt die \'Lokale Möbelsuche\' probieren!',
    'try_baby'              => 'Jetzt \'Baby Schnäppchensuche\' probieren!',
    'try_local'             => 'Jetzt die \'Lokale Schnäppchensuche\' probieren!',
    'try_rightnow'          => 'Jetzt die \'Endet sofort\' Suche probieren!',
    'try_unwanted'          => 'Jetzt die \'Ungewollte Geschenke\' Suche probieren!',
    'try_joblot'            => 'Jetzt die \'Restposten\' Suche probieren!',
    'try_misspelled'        => 'Jetzt die \'Schreibfehler im Titel\' Suche probieren!',
    'try_laptop'            => 'Jetzt die \'Lokale Laptop\' Suche probieren!',
    'try_rightnow'          => 'Jetzt die \'Endet sofort\' Suche probieren!',
    'try_carparts'          => 'Jetzt die \'Lokale Auto-Ersatzteil\' Suche probieren!',
    'try_cars'              => 'Jetzt die \'Lokale Auto\' Suche probieren!',
    'try_bin'               => 'Jetzt die \'Buy It Now\' Suche probieren!',
    'try_buynow'            => 'Jetzt die \'Buy It Now\' Suche probieren!',

    #common items
    'quick_search'          => 'Schnellsuche',

    'desc_short_moresearches_title' => 'Mehr Suchparameter',

    #descriptions


    #home
    'desc_long_home_title' => 'Richtig Einkaufen mit Schnäppchensuche',
    'desc_long_home1'      => 'Schnäppchensuche macht es einfach die besten Angebote im Internet zu finden.'.
                                ' Wenn sie ein leidenschaftlicher Schnäppchenjäger sind und nicht genug '.
                                'Schnäppchen finden können dann sind sie jetzt genau am richtigen Ort!',
    'desc_long_home2'      => 'Unsere spezielle Schnäppchensuche bringt sie direkt zu den Angeboten die sie sehen '.
                                'wollen. Ein einziger Klick findet eBay Auktionen die '.
                                '<a href="?page=rightnow">genau jetzt enden</a>,
                               <a href="?page=nightowl">Nachtschwärmer Schnäppchen</a>,
                               <a href="?page=misspelled"> Tippfehler Schnäppchen</a>,
                               <a href="?page=unwanted">ungewollte Geschenke</a>,
                               <a href="?page=local">Schnäppchen in ihrer Nähe</a>
                               oder finden sie super Schnäppchen auf <a href="?page=amazon">Amazon</a>.
                               Mit  der Schnäppchensuche sind die '.
                                'besten Angebote des Internets immer zur Hand.',
    'desc_long_home3'      => 'Sind sie bereit? Dann starten sie jetzt indem sie '.
                                'eine unserer Suchmethoden unten anklicken ',
    'desc_long_home4'      => '<ul><li>Benutzen sie die Suchfunktionen um Versandkosten einzusparen
                               und großartige Angebote mit <a href="?page=local">lokalen Schnäppchen</a>
                               auf eBay zu finden.<li><a href="?page=misspelled">Auktionen mit Tippfehlern</a>
                               haben oft nur wenige Gebote. Wenn Sie es richtig '.
                                'schreiben können dann können sie sich ein
                               paar tolle Schnäppchen angeln.</li><li>Geschmäcker sind verschieden.
                               Manch <a href="?page=unwanted">ungewollte Geschenke</a> sind vielleicht '.
                                'genau was sie haben wollen.</li><li>Unsere '.
                                '<a href="?page=nightowl">Nachtzeit Suche</a> findet Auktionen '.
                                'die mitten in der Nacht enden.</li>
                               <li>Bleiben Sie am Ball denn so manche Auktion '.
                                '<a href="?page=rightnow">endet Jetzt</a> und kaum jemand '.
                                'hat etwas dafür geboten</li></ul>',
    'desc_long_home5'      => 'Wir sammeln keine personenbezogenen Daten über unsere Benutzer.
                                Suchen bringen sie einfach und direkt zu den Schnäppchen.
                                 ist eine zertifizierte eBay kompatible Anwendung.
                                Lesen sie die <a href="?page=faq">häufig gestellten Fragen</a>
                                oder klicken sie um mehr über herauszufinden.',
    'desc_long_home6'      => '',

    #aboutus
    'desc_short_about_title'=> 'Über Uns',
    'desc_long_about_title' => 'Über Uns',
    'desc_long_about1'      => 'Tom und Bry, die Gründer und Eigner von BayCrazy haben etwas gemeinsam:
                                 Sie sind beide Schnäppchenjäger! In 2008, Steckten sie ihre Köpfe zusammen '.
                                'und schrieben eine Reihe spezieller Suchmaschinen die,
                                 die besten Schnäppchen auf eBay und Amazon aufspüren.',
    'desc_long_about2'      =>  '<h3>Suchfunktionen finden die besten Angebote im Internet</h3>',
    'desc_long_about3'      =>  'Jeder liebt Schnäppchen, aber sie im Internet aufzustöbern ist nicht immer einfach.
                                 Suchfunktionen sind speziell daraus ausgerichtet Schnäppchen zu finden die
                                 sonst leicht in der Masse von Angeboten untergehen. '.
                                'Unsere Suchfunktionen sind eine von eBay genehmigte Anwendung die die hohen '.
                                'Qualitätsstandards des Unternehmens erfüllt.',
    'desc_long_about4'      =>  'Schnäppchen zu jagen kann einen unerfahrenen Einkäufer schnell in den Wahnsinn
                                 treiben. Die Suchfunktionen hingegen machen es einfach für '.
                                'unsere Besucher die besten Angebote im Internet zu ergattern. Aber auch '.
                                'erfahrenen Schnäppchenjägern verschaffen die zusätzlichen Suchfunktionen
                                 große Vorteile bei der Suche nach den großen Ersparnissen.',
    'desc_long_about5'      =>  '',
    'desc_long_about6'      =>  'spezialisiert in Suchkriterien die andere Einkäufer übersehen.
                                Zum Beispiel Auktionen die <a href="/?page=rightnow">jetzt enden</a> oder
                                 <a href="/?page=nightowl">Nacht Auktionen</a> die enden wenn niemand Wach ist.
                                 Auch eine Suche für <a href="/?page=misspelled">Auktionen mit Schreibfehlern</a>'.
                                ' bietet an.',
    'desc_long_about7'      =>  'Da nichts verkauft ist eine Registration nicht nötig.
                                 Sie bleiben völlig anonym während sie nach den besten Schnäppchen jagen.
                                 Wenn Sie eine Postleitzahl eingeben wird diese nur dazu verwendet '.
                                'die besten Suchergebnisse zu finden.
                                 Ihre Emailaddresse speichern wir nur wenn sie uns kontaktieren.
                                 Finden sie unsere <a href="/?page=privacy">vollständige '.
                                'Datenschutzerklärung hier.</a>',
    'desc_long_about8'      =>  'Mit BC macht Schnäppchen jagen Spaß.
                                 Wählen Sie einfach eine Suchfunktionen im Menu oben aus oder
                                 benutzen Sie unsere Schnellsuche. ',

    #privacy
    'desc_long_privacy_title' => 'Datenschutzerklärung',
    'desc_long_privacy1'      => '<h3>Zielsetzung der Seite</h3>',
    'desc_long_privacy2'      => 'BayCrazy.de existiert um Nutzern zu helfen Schnäppchen auf eBay und Amazon zu finden.
                                  Dafür erhalten wir von diesen Seiten eine Vermittlungsgebühr.
                                  BayCrazy.de ist eine unabhängige Suchseite und ist nicht Teil von eBay.',
    'desc_long_privacy3'      => '<h3>Datenschutz</h3>',
    'desc_long_privacy4'      => 'Wir verwenden keinerlei '.
                                    '<a href=”http://de.wikipedia.org/wiki/Personenbezogene_Daten”
                                   target="pii">Personenbezogene Daten</a> über unsere Benutzer.',
    'desc_long_privacy5'      => '<h3>Nutzung von Cookies</h3>',
    'desc_long_privacy6'      => 'Cookies erlauben es uns Ihre Einstellungen, wie z.B. Sprache und '.
                                    'Postleitzahl zu speichern. Wir benutzen Cookies für ein '.
                                    'reibungsloses Sucherlebnis. Wenn Sie keine cookies benutzen '.
                                    'wollen werfen Sie bitten einen Blick auf
                                   <a href="http://www.aboutcookies.org.uk/managing-cookies">aboutcookies.org.uk</a>.
                                   Diese Seite enthält Informationen darüber wie sie Cookies in allenüblichen '.
                                    'Browsern abschalten können. Wenn Sie Cookies abschalten wird die '.
                                    'Nutzerfreundlichkeit der Seite negativ beeinflusst.',
    'desc_long_privacy7'      => '',

    # Contact Page
    'desc_long_contact_title' => 'Kontakt',
    'desc_long_contact1'      => 'Danke für Ihren Besuch auf BayCrazy.de.
                                   Wir hoffen Sie hatten Spaß bei der Schnäppchenjagd mit unseren Suchfunktionen.',
    'desc_long_contact2'      => 'Bitte lassen Sie uns mit dem Kontaktformular unten wissen wie '.
                                    'unsere Suchfunktionen für eBay und Amazon ihnen bei der '.
                                    'Schnäppchenjagd behilflich waren.',
    'desc_long_contact3'      => '<ul><li>Wie erfolgreich war Ihre Suche? Wir sind immer an '.
                                    'Erfolgsgeschichten interessiert.</li>
                                   <li>Was können wir verbessern? Lassen sie es uns wissen und '.
                                    'wir werden daran arbeiten</li>
                                   <li>Haben sie falsche oder kaputte Links gefunden? Sagen Sie uns '.
                                    'wo und wir werden sie schnellstmöglich reparieren.</li>
                                   <li>Haben Sie eine Frage die unsere<a href="?page=faq">Häufig '.
                                    'gestellten Fragen</a> nicht beantworten?
                                   Fragen Sie uns und wir werden unser Bestes tun sie zu beantworten.</li></ul>',
    'desc_long_contact4'      => 'Sie können uns mit ihren Daten vertrauen.Datenschutz ist uns wichtig und '.
                                    'wir werden ihre Informationen
                                   niemals zu einem Zweck als dem von Ihnen Gewählten verwenden.
                                   Lesen Sie <a href="?page=privacy">hier unsere vollständige '.
                                    'Datenschutzerklärung</a>.',

    'desc_long_contactinfo'    => 'BayCrazy Ltd. (Firmennummer 07820359) Registriert in England und '.
                                    'Wales nach britischem Gesetz firmiert unter dem  Namen '.
                                    'BayCrazy.com und Baycrazy.de',

    'contact_thanks'        => 'Vielen Dank für Ihre Anfrage',
    'contact_acknowledge'   => 'Ihre Nachricht wurde nun an den Webmaster versendet!',
    'contact_nomessage'     => 'Sie müssen eine Nachricht eingeben. '.
                                'Versuchen Sie es noch einmal.',
    'contact_noemail'       => 'Sie haben keine Email Adresse angegeben: Wir '.
                                'werden Ihre Nachricht erhalten, können Ihnen aber nicht antworten.',
    'contact_close'         => 'Dieses Formular schließen',
    'contact_title'         => 'Wir hören immer gerne Ihre Rückmeldung, Kritik, Vorschläge , '.
                                'oder auch Lob.',
    'contact_faqtitle'      => 'Bitte lesen Sie zuerst die folgenden häufig gestellten Fragen '.
                                'und Antworten um Ihnen und uns etwas Zeit zu sparen!',
    'contact_qsell'         => 'Q: Wie kann ich verkaufen?',
    'contact_asell'         => 'A: Ihre ist eine Suchmaschine '.
                                'um Käufern Artikel aufzuzeigen, die unter Wert auf eBay angeboten werden '.
                                'Wir bieten keinen Verkauf an.',
    'contact_qjoin'         => 'Q:Wie kann ich Mitglied bei BC werden ?',
    'contact_ajoin'         => 'A: Das ist nicht notwendig! '.
                                 'Wichtig, um vollen nutzen zu ziehen sollten sie sich bei '.
                                 '<a href=\'https://scgi.ebay.de/ws/eBayISAPI.dll?RegisterEnterInfo\' '.
                                    'target=\'registerebay\'>'.
                                 'ebay.de</a> ein Benutzerkonto erstellen. ',
    'contact_qbid'          => 'Q: Ich habe mehr geboten als ich eigentlich wollte, kann '.
                                'ich das Gebot zurücknehmen?',
    'contact_abid'          => 'A: Sie müssen den eBay Regeln folgen. Eine Anleitung finden '.
                                'Sie hier <a href=\'http://pages.ebay.de/help/buy/questions/'.
                                'retract-bid.html\' target=\'retract\'>HIER.</a> Handeln Sie sofort.',
    'contact_text1'         => 'Alle Felder in diesem Formular sind freiwillig und '.
                                'jegliche angegebene Informationen werden streng unter Berücksichtung unserer '.
                                '<a href="search.php?page=privacy" '.
                                'target="Privacy">Datenschutzerklärung</a> behandelt',
    'contact_text2'         => 'Wir werden Ihre Kontaktdaten nur verwenden, wenn Sie '.
                                '"Bitte kontaktieren Sie mich" auswählen',
    'contact_name'          => 'Ihr Name:',
    'contact_email'         => 'Ihre Email Adresse:',
    'contact_p2'            => 'Anliegen:',
    'contact_contactme'     => 'Bitte kontaktieren Sie mich:',
    'contact_message'       => 'Ihre Nachricht:',
    'contact_send'          => 'Nachricht senden',

    #faq
    'desc_long_faq_title' => 'Häufig gestellte Fragen',
    'desc_long_faq1'      => 'Wenn Sie Fragen oder Hilfe '.
                                 'Seiten benötigen lesen Sie sich bitte unsere '.
                                 'Häufig gestellte Frage Sektion durch. ',
    'desc_long_faq2'      => 'Um Antworten anzuzeigen klicken Sie bitte auf die jeweilige Frage.',
    'desc_long_faq3'      => 'Wenn Sie hier keine Antwort finden können, benutzen Sie bitte unsere '.
                                 '<a href="/content/contact">Kontakt</a> Seite. ',
    'faq_q1'              => 'F: Wie verkaufe ich etwas? ',
    'faq_a1'              => 'A: Das ist nicht möglich. Alle Schnäppchen die Sie '.
                                 'finden können, werden von Verkäufern auf eBay '.
                                 'und Amazon verkauft. Wir akzeptieren keine direkten Einträge. ',
    'faq_q2'              => 'F: Wie werde ich Mitglied? ',
    'faq_a2'              => 'A: Das ist nicht nötig! Die Nutzung von <a href="www.bayCrazy.de">'.
                                 'BayCrazy</a>erfolgt anonym. Um den größten Nutzen aus unserer '.
                                 'Seite zu ziehen sollten Sie sich aber bei eBay oder Amazon anmelden. ',
    'faq_q3'              => 'F: Was kostet die Nutzung ihre',
    'faq_a3'              => 'A: Nichts! Sie können alle Suchfunktionen umsonst nutzen. ',
    'faq_q4'              => 'F: Ich biete etwas auf eBay an aber es taucht nicht auf. Warum? ',
    'faq_a4'              => 'A: Wir zeigen in unseren Resultaten Artikel die wahrscheinlich '.
                                 'Schnäppchen sind. Wenn es so aussieht als würde eine Auktion viele Bieter '.
                                 'anziehen ist sie für unsere Nutzer weniger interessant. '.
                                 'Aus diesem Grund zeigen wir nicht alle Auktionen an. ',
    'faq_q5'              => 'F: Auktionen die ich mir angesehen oder darauf geboten habe sind '.
                                 'nicht mehr  zu finden. Warum nicht? ',
    'faq_a5'              => 'A: Wenn eine Auktion so aussieht als ob sie kein Schnäppchen mehr sein wird, '.
                                 'zeigen wir sie unter Umständen nicht mehr an. '.
                                 'Wenn sie auf etwas geboten haben, sollten sie ihr E-mail Postfach und ihre '.
                                 '\'Mein Ebay\' Sektion auf eBay kontrollieren. ',
    'faq_q6'              => 'F: Ich habe ein falsches Gebot abgegeben. Was soll ich tun?',
    'faq_a6'              => 'A: Sie sollten umgehend eBay mit '.
                                 '<a href="http://pages.ebay.de/help/policies/invalid-bid-retraction.html" '.
                                 'target="_blank">dieser Prozedur</a> benachrichtigen. ',
    'faq_q7'              => 'F: Ich habe auf eine Auktion geboten aber der Anbieter will mich '.
                                 'die Ware nicht abholen lassen. Was soll ich tun? ',
    'faq_a7'              => 'A: Leider können wir Ihnen hierbei nicht behilflich sein. '.
                                 'Gebote auf eBay sind eine Angelegenheit zwischen dem Käufer, Anbieter und eBay. '.
                                 'Wir empfehlen ihnen in diesem Fall einen Blick auf '.
                                 '<a href="http://resolutioncentre.ebay.de/" target="resolution">'.
                                 'eBay\'s Probleme klären Seite</a> zu ',
    'faq_q8'              => 'F: Wo ist der Haken? ',
    'faq_a8'              => 'A: Die Seite erhält eine kleine '.
                                 'Vermittlungsgebühr wenn sie Käufer und neue Nutzer zu eBay '.
                                 'und Amazon schickt. ',
    'faq_q9'              => 'F: BC warnt mich dass meine Postleitzahl falsch ist. Warum? ',
    'faq_a9'              => 'A: Der häufigste Grund für dies ist dass sie den Buchstaben \'O für Oskar\' '.
                                 'anstatt einer Null eingegeben haben. Sie sollten auch überprüfen dass '.
                                 'Sie das korrekte Land oben rechts ausgewählt haben. ',
    'faq_q10'             => 'F: Ich gebe eine Suche ein bekomme aber keine Resultate. Warum? ',
    'faq_a10'             => 'A: Höchstwahrscheinlich gibt es keine Auktionen die mit ihren Suchparametern '.
                                 'übereinstimmen. Gehen Sie sicher Sie haben die korrekte '.
                                    'Kategorie oder \'Alle Kategorien\' '.
                                 'ausgewählt. Versuchen sie die Maximalwerte für Preis oder die '.
'Entfernung zu erhöhen. ',
    'faq_q11'              => 'F: Die Entfernungen die Sie angeben sind falsch. Warum? ',
    'faq_a11'              => 'A: Wir geben die ungefähre Entfernung in Luftlinie an. '.
                                 'Der reale Anfahrtsweg kann davon abweichen. ',
    'faq_q12'              => 'F: Können Sie mir mehr über einen Artikel erzählen den Sie verkaufen?',
    'faq_a12'              => 'A: BC verkauft keine Waren selbst.Die angezeigten Ergebnisse werden '.
                                 'von Verkäufern auf eBay oder Amazon angeboten. Sie sollten diese kontaktieren. '.
                                 'Normalerweise sollte dies möglich sein indem Sie auf den '.
                                    'Namen des Anbieters klicken. ',
    'faq_q13'              => 'F: Ich möchte eine Ihrer Waren als \'Sofort Kauf\' erstehen?',
    'faq_a13'              => 'A: BC verkauft keine Waren selbst. Die angezeigten Ergebnisse '.
                                 'werden von Verkäufern auf eBay oder Amazon angeboten. '.
                                    'Sie sollten diese kontaktieren. '.
                                 'Normalerweise sollte dies möglich sein indem Sie auf '.
                                    'den Namen des Anbieters klicken. ',
    'faq_q14'              => 'F: Ich habe etwas gekauft, habe aber nichts vom Anbieter gehört. '.
                                    'Wie bezahle Ich? Wo ist meine Ware.',
    'faq_a14'              => 'A: Die angezeigten Ergebnisse werden von Verkäufern '.
                                    'auf eBay oder Amazon angeboten. '.
                                  'Sie sollten diese kontaktieren. Normalerweise '.
                                    'sollte dies möglich sein indem Sie auf den '.
                                  'Namen des Anbieters klicken. ',
    'faq_q15'              => 'F: Ich kann nicht in BC einloggen. Was soll Ich tun? ',
    'faq_a15'              => 'A: BC hat keine Benutzerkonten! Sie sollten dies '.
                                 '<a href="https://signin.ebay.de/ws/eBayISAPI.dll?SignIn" '.
                                 'target="ebaylogin">auf dieser Seite</a> tun.',
    'faq_q16'              => 'F: Jemand hat mein Benutzerkonto übernommen. Was soll ich tun? ',
    'faq_a16'              => 'A: BC hat keine Benutzerkonten! Falls das Problem ihr eBay '.
                                 'Benutzerkonto betrifft folgen Sie bitte '.
                                 '<a href="http://pages.ebay.de/help/account/securing-account.html '.
                                 'target="hackedebay">diesem Link.</a> ',
    'faq_q17'              => 'F: Wer führt BC? Ist es Teil von eBay? ',
    'faq_a17'              => 'A: BayCrazy.de ist eine unabhängiges Suchangebot. Es gehört der Firma BayCrazy Ltd. '.
                                 'Welche in Großbritannien unter der Firmennummer 07820359 registriert ist. '.
                                 'BayCrazy gehört in keiner Weise zum eBay Konzern. ',
    'faq_q18'              => 'F: In welchen Ländern ist BC verfügbar? ',
    'faq_a18'              => 'A: Der Firmensitz von BC Ltd. Ist in Großbritannien. '.
                                 'Wir bieten im Moment Suchangebote für die eBay Seiten in Großbritannien, die USA, '.
                                 'Deutschland und Australien. ',
    'faq_q19'              => 'F: Ist BC sicher?',
    'faq_a19'              => 'A: Selbstverständlich! Wir agieren innerhalb strikter gesetzlicher Vorschriften '.
                                 'einschließlich aller Britischen Vorschriften für registrierte Firmen. '.
                                 'Wir werden von eBay und deren Partner ROEYE regelmäßig überprüft. ',
 #   'faq_q20'              => 'F: Schickt BC mir unerwünschte emails und \'Spam\'?',
 #   'faq_a20'              => 'A: BayCrazyspeichert im Moment keinerlei email Adressen von Nutzern außer '.
 #                                'wenn diese uns zuerst kontaktieren. In diesen Fällen '.
 #                                   'werden wir sie nur kontaktieren '.
 #                                'um Ihnen die gewünschten Informationen zu geben. '.
 #                                'In seltenen Fällen würden wir Sie auch zu eingeschränkten '.
 #                                'Marktforschungszwecken kontaktieren. '.
 #                                'Sollten Sie uns mitteilen dass Sie nicht kontaktiert werden möchten werden wir '.
 #                                'uns daran halten. ',
#    'faq_q21'              => 'Q: Benutzt BayCrazy Cookies? ',
#    'faq_a21'              => 'A: BayCrazy benutzt Cookies. Nur so ist es möglich '.
#                                    'Ihnen den besten Service zu bieten. '.
#                                 'Zum Beispiel in dem wir ihre Sucheinstellungen speichern. '.
#                                 'Wir teilen diese Cookies nicht mit anderen Seiten. '.
#                                    'Die Seiten auf die wir verlinken, '.
#                                 'wie Z.B. Ebay, verwenden wahrscheinlich auch Cookies aber dies '.
#                                    'ist ausserhalb unserer Kontrolle. ',
    'faq_q22'              => 'Q: Welche Daten speichert BayCrazy über mich und meine Nutzung von eBay und Amazon? ',
    'faq_a22'              => 'A: Wir speichern die Daten die Sie in unsere Formulare eingeben z.B. Postleitzahlen. '.
                                 'Wie die meisten Webseiten speichern wir auch die Internetadressen von denen aus auf '.
                                 'unsere Webseite zugegriffen wird. Wenn ihr Browser uns seine Version mitteilt '.
                                 'wissen wir unter Umständen ob Sie die Seite mit '.
                                    'einem mobilen Gerät aufgerufen haben. '.
                                 'Von Zeit zu Zeit teilen uns eBay und Amazon '.
                                    'Informationen über Verkäufe mit die durch '.
                                 'uns vermittelt wurden. Keine dieser '.
                                    'Informationen sind dazu geeignet sie persönlich zu identifizieren. ',
    'faq_q23'              => 'Q: Meine Frage wird hier nicht beantwortet. Was soll ich tun? ',
    'faq_a23'              => 'A: Bitte kontaktieren Sie uns über unsere '.
                                    '<a href="/content/contact">Kontakt</a> Seite. '.
                                 'Bitte beschreiben Sie Ihr Problem vollständig und es ist meistens hilfreich ihre '.
                                 'Postleitzahl und ihr Land anzugeben. Wir könne Ihnen nur antworten wenn Sie '.
                                 'ihre korrekte Emailaddresse angeben. '.
                                    'Wir können Sie nicht über ihren eBay Nutzernamen identifizieren. '.
                                 'Deswegen sollten sie in jedem Fall eine korrekte Emailaddresse angeben. ',
    'faq_q24'              => 'Q: Ich habe einen Vorschlage für Ihre Seite. Wie kann ich Sie kontaktieren?',
    'faq_a24'              => 'A: Wir freuen uns über Anregungen und Vorschläge. '.
                                 'Bitte kontaktieren Sie uns über unsere '.
                                    '<a href="/content/contact">Kontakt</a> Seite.',

    #local
    'desc_long_local_title' => 'Lokale Schnäppchen : eBay lokale Schnäppchensuche ',
    'desc_long_local1'      => 'Diese Suchfunktion findet Artikel die der Verkäufer nicht versenden will, '.
                                'die sich aber in der Nähe Ihres Wohnorts befinden. ',
    'desc_long_local2'      => 'Der Verkäufer hat seinen Radius begrenzt und wird nur wenige '.
                                'Bieter anziehen. <b>Wenige Bieter bedeuten kleinere Preise!</b> '.
                                'Alles was Sie noch zutun haben, '.
                                    'ist beim Verkäufer vorbeizufahren und den Artikel für '.
                                'ein paar Cent einzusammeln. ',
    'desc_long_local3'      => 'Das Team besteht aus leidenschaftlichen '.
                                'Schnäppchenjägern die das lokale Schnäppchen '.
                                    'Suchsystem entwickelt haben '.
                                'um einfach und schnell Artikel zu finden die '.
                                    'günstig in Ihrer Nachbarschaft angeboten werden! '.
                                'Einfach Postleitzahl eingeben, Entfernung auswählen '.
                                    'und sehen Sie selbst wie viele Artikel '.
                                'zu Selbstabholung nur darauf warten gefunden zu werden '.
                                'Die Suchmaschine ist kostenlos und keine Registrierung '.
                                    'oder Anmeldung ist notwendig. ',
    'desc_long_local4'      => 'verwendet spezielle Filter, '.
                                    'die genial die besten Schnäppchen '.
                                'heraussuchen, die der Verkäufer fast aus dem Fenster schmeißt, spottbillig '.
                                'und in Ihrem Dorf oder Ihrer Stadt! ',
    'desc_long_local5'      => 'Wie funktioniert es also? Unsere lokale '.
                                'Schnäppchensuchfunktion stöbert durch eBay und schaut '.
                                    'nach verräterischen Zeichen die implizieren, '.
                                'dass der Verkäufer den Artikel nicht versenden will. '.
                                    'Indem er nur Selbstabholung anbietet oder '.
                                'die Versandgebühren zu hoch sind, vertreibt er mögliche Käufer, '.
                                'spriche weniger Bieter für diesen Artikel.',
    'desc_long_local6'      => '<strong>Wenige Bieter bedeuten kleine Preise! </strong>'.
                                'Auf eBay befinden sich hunderte Artikel die für einen '.
                                    'Euro ohne Mindestgebot zu haben sind, sie warten  '.
                                'nur darauf gefunden und abgeholt zu werden. ',
    'desc_long_local7'      => 'Warum bietet ein Verkäufer \'nur Selbstabholung an?\''.
                                'Nun ja, dafür gibt es einige Gründe: Der Artikel ist '.
                                    'vielleicht einfach zu groß zum senden, '.
                                'zu zerbrechlich oder zu wertvoll. Zum Beispiel Laptops, '.
                                    'Kleiderschränke, Fernseher, '.
                                'Waschmaschinen, Vorhänge, Spiegel, Schmuck etc. sind oft  Top Artikel '.
                                'in dieser Kategorie.',
    'desc_long_local8'      => 'Vielleicht ist dem Verkäufer auch egal wie viel Sie bezahlen '.
                                'werden: Vielleicht will jemand einfach nur Platz '.
                                    'im Keller oder auf dem Dachboden schaffen. '.
                                'Was auch immer der Grund sein mag, für Sie heißt das tolle Schnäppchen '.
                                'zu Hammerpreisen. Worauf warten Sie noch? Versuchen kostet ja nichts!',
    'desc_long_local9'      => 'Natürlich bieten wir Extra Suchfunktionen an '.
                                'um die Suche zu verfeinern. Diese erweiterte Suche '.
                                    'öffnet sich im nächsten Fenster, '.
                                'wenn Sie auf \'Schnellsuche\' klicken',
    'desc_short_local_title'=> 'Lokale Schnäppchensuche',
    'desc_short_local1'     => 'Diese Option findet schwere, zerbrechliche oder sperrige '.
                                'Artikel, die der Verkäufer wahrscheinlicht nicht verschicken will,'.
                                'die aber in Ihrer Nähe sind. Der Verkäufer '.
                                    'hat seine Zielgrupper stark limitiert '.
                                'und wird wenige Bieter erreichen. Süe müssen '.
                                    'vielleicht einfach nur zum Verkäufer fahren '.
                                'und den Artikel für ein paar Cent abholen.',
    'desc_short_local2'     => 'Häufige Wahnsinnsschnäppchen sind Möbel, '.
                                'Pc\'s, Fernseher, Sportgeräte, Alufelgen, etc. ',
    'desc_tiny_local'       => 'Schnäppchen in Ihrer Nähe!',
    /**
     * @todo: translate
     */
    'desc_tabs_local'       => '<h2>Lokale Schnäppchen</h2>
        <p>Search Now for bargains near you, which either can\'t be
        sent in the mail, or will cost loads! So they\'ll probably
        go dirt cheap.</p>
        <h3>Save Time, Buy Local</h3>
        <p>Just enter your postcode and search now</p>',

    'formhelp_local'        => 'Just fill in your postcode...',


    'desc_long_misspelled_title'=> 'Verrückte Schreibfehler : Rechtschreibfehler im Titel Suche',
    'desc_long_misspelled1'     => 'Diese Option findet Artikel bei denen der Verkäufer  '.
                                    'wichtige Schlagwörter in der Beschreibung falsch geschrieben hat.',
    'desc_long_misspelled2'     => 'Andere Käufer finden diese Artikel kaum und Sie könnten  '.
                                    'der einzige Bieter sein und ein Schnäppchen machen. :o)',
    'desc_long_misspelled3'     => 'Unsere super Schreibfehler Suche ist der '.
                                    'schnellste und einfachste Weg einige der am häufigsten übersehenen, '.
                                    'viel angebotenen und sehr begehrten Artikel auf eBay zu finden. ',
    'desc_long_misspelled4'     => 'Die Suche ist super einfach. Sie müssen einfach nur '.
                                    'darauf achten den Suchbegriff richtig zu schreiben. Wir suchen dann für Sie '.
                                    'die häufigsten Schreibfehler heraus. Unsere clevere Suchemaschine findet '.
                                    'den Fehler des Verkäufers, nicht ihren!',
    'desc_long_misspelled5'     => 'Wir haben für unsere Nutzer schon tausende '.
                                    'von Schnäppchen gefunden, Tiefstpreise, aus dem simplen Grund, dass '.
                                    'der Artikel falsch beschrieben wurde.',
    'desc_long_misspelled5'     => 'Wir haben für unsere Nutzer schon tausende '.
                                    'von Schnäppchen gefunden, Tiefstpreise, aus dem simplen Grund, dass '.
                                    'der Artikel falsch beschrieben wurde.',
#    'desc_long_misspelled6'     => 'Ohne die Hilfe von BayCrazy sind viele dieser Schätze '.
#                                    'kaum auffindbar - Aber wir helfen IHNEN diese zu finden.',
#    'desc_long_misspelled7'     => 'Nehmen Sie zum Beispiel Nintendo, Schreibfehler '.
#                                    'wie nintndo, Ninntendo, nntendo, nintedo etc. '.
#                                    'haben schon oft dazu geführt, dass kaum oder garnicht geboten wurde oder '.
#                                    'unglaublich niedrige Preise entstanden.',
#    'desc_long_misspelled8'     => 'Andere häufig falsch geschrieben Wörter sind unter anderem '.
#                                    'Playstation, Wii, Ikea, Habitat, Laura Ashley, Manolo, Chanel, '.
#                                    'Zanussi, Abercrombie, und viele weitere Markennamen! Geben Sie '.
#                                    'einfach einen oder zwei Suchbegriffe ein oder noch besser einen Markennamen, '.
#                                    '<span class="red"> (Wichtig: RICHTIG schreiben)</span> '.
#                                    'Klicken Sie dann auf den \'Schnellsuche\' Button.',
#    'desc_long_misspelled9'     => 'Natürlich gibt es weitere Suchoptionen um die Suche '.
#                                    'zusätzlich zu verfeinern. Diese Auswahl finden Sie auf der nächsten Seite '.
#                                    'klicken Sie einfach auf \'Schnellsuche\'',
#
    'desc_short_misspelled_title'=> 'Schreibfehler Suche',
    'desc_short_misspelled1'     => 'Diese Option findet Artikel, die von achtlosen Verkäufern '.
                                     'mit Scheibfehlern in wichtigen Schlagwörtern eingestellt wurden. '.
                                    'Andere Käufer '.
                                     'finden diese Artikel erst garnicht, Sie könnten also der '.
                                    'einzige Bieter sein und '.
                                     'ein wahnsinns Schnäppchen machen. :o)',
    'desc_short_misspelled2'     => '<span class="red"> Sie MÜSSEN richtig '.
                                     'buchstabieren :o) </span>',
    'desc_tiny_misspelled' => 'Artikel finden, die sonst keiner sieht!',

    'desc_long_rightnow_title'  => 'Sofort endende Auktionen : Artikel mit Auktion Funktion '.
                                    'die bald enden, komplett ohne Gebote',
    'desc_long_rightnow1'       => 'Schauen Sie sich jetzt eBay Auktionen an '.
                                    'die SOFORT! Enden und noch keine Gebote erhalten haben!',
    'desc_long_rightnow2'       => 'Einige Angebote werden aus verschiedenen Gründen '.
                                    'einfach übersehen. Doch wir finden diese ungewollten Artikel für Sie! '.
                                    '- Artikel die bald enden aber noch kein einziges Gebot haben!',
    'desc_long_rightnow3'       => 'Unser cleveres Team hat sich entschlossen '.
                                    'eine Suchfunktion zu entwickeln, die Schnäppchenjäger mit '.
                                    'erfolglosen Verkäufern zusammenführt. Doch anders wie Sie denken.',
    'desc_long_rightnow4'       => 'Diese Suchfunktion ist schnell und macht Spaß, von alltäglichen bishin '.
                                    'zu außergewöhnlichen Angeboten werden '.
                                    'Sie eine bunte Liste finden, alle Artikel warten nur auf '.
                                    'einen Käufer - haben jedoch noch keine Bieter angezogen außer Ihnen, '.
                                    'somit stehen Sie außer Konkurrenz! Ohne andere Bieter '.
                                    'können Sie häufig übersehen Schnäppchen machen, für nur einige Euro!',

    'desc_short_rightnow_title' => 'Endet Jetzt!',
    'desc_short_rightnow1'      => 'Schauen Sie sich die SOFORT ENDENDEN Auktionen an! '.
                                    'Null Gebote bisher! Es wird ein heißes Rennen gegen die Zeit um diese '.
                                    'Artikel zu erwerben die von anderen Käufern offensichtlich '.
                                    'übersehen wurden.',
    'desc_short_rightnow2'      => 'Schauen Sie sich SOFORT ENDENDE Angebote jetzt an!!',
    'desc_tiny_rightnow'        => 'Letzte Minute, noch keine Gebote!',

    'desc_long_nightowl_title'  => 'Verrückte Uhrzeit : Angebote, die mitten in der Nacht enden ',

    'desc_long_nightowl1'       => 'Manche verrückte, achtlose Verkäufer stellen ihre Angebote '.
                                    'ein ohne darauf zu achten, wann die Auktion enden wird, warum auch immer '.
                                    'Und wenn diese Endzeit nun mitten in der Nacht liegt, '.
                                    'werfen sie ihre Artikel fast aus dem Fenster. ',
    'desc_long_nightowl2'       => 'Der schlaue Käufer, damit meinen wir Sie, kann '.
                                    'einfach vor dem Schlafen gehen durch die Auktionen stöbern und auch gleich '.
                                    'bieten. Und wenn Sie aufwachen stellen Sie begeistert fest, dass '.
                                    'Sie im Schlaf ein Schnäppchen ergattert haben. '.
                                    'Den Verkäufer wird es vielleicht ärgern, '.
                                    'aber wenigstens hat er einen Käufer gefunden.',
    'desc_long_nightowl3'       => 'Diese einfache Suche, ist der beste Weg Artikel auf '.
                                    'eBay zu finden, die enden, wenn die meisten von uns tief und fest '.
                                    'schlafen. Ohne mögliche wache Käufer gibt es für den Verkäufer '.
                                    'ein böses erwachen nämlich keinen Verkauf. '.
                                    'Es sei denn natürlich, Sie nutzen unsere Verrückte Zeiten Suche um '.
                                    'im Traum ein tolles Schnäppchen zu ersteigern!',
    'desc_long_nightowl4'       => 'Sie müssen nicht bis spät in die Nacht wach bleiben, '.
                                    'wir helfen Ihnen vorher bei der Suche. '.
                                    'Dann müssen Sie nur noch vor dem Schlafen gehen '.
                                    'bieten und morgens zu Ihrem Schnäppchen aufwachen.',

    'desc_short_nightowl_title' => 'Mitternachts Specials',
    'desc_short_nightowl1'      => 'Diese Suchfunktion finden Angebote, deren Auktionen '.
                                    'vom Verkäufer so eingestellt wurden, '.
                                    'dass sie mitten in der Nacht enden, dann, wenn  '.
                                    'niemand beobachtet oder bietet. Umso weniger Bieter, '.
                                    'desto kleiner der Preis.',
    'desc_short_nightowl2'      => 'Bieten Sie bevor Sie ins Bett gehen und freuen Sie sich am Morgen '.
                                    'über Ihr Schnäppchen.',
    'desc_tiny_nightowl'        => 'Angebotsende wenn Andere schlafen!',

    #unwanted
    'desc_long_unwanted_title'  => '<font color=\'red\'>NEU!!! </font>Ungewollte Geschenke Suchen'.
                                    'Geschenksuche : Zu verkaufen von undankbaren Freunden.',
    'desc_long_unwanted1'       => 'Diese Suchfunktion findet Artikel, die der Verkäufer '.
                                    'einfach nur loswerden und zu Geld machen will, z.B. alte Geschenke.',
    'desc_long_unwanted2'       => 'Vielleicht kennt der Verkäufer den wahren Wert des Gegenstandes nicht, oder '.
                                    'es ist ihm einfach egal.',
    'desc_long_unwanted3'       => 'Mal ehrlich: Wir kennen es doch alle. Wir alle wissen '.
                                    'wie es ist, wenn man ein Geschenk bekommt, das man nicht braucht, '.
                                    'schon hat oder einfach nur hasst. Sehr oft landen diese ungewollten Geschenke '.
                                    'dann bei eBay, wo der arme Beschenkte sie wenigstens zu ein wenig  '.
                                    'Geld machen kann. Hier warten diese Geschenke auf Sie! Jemand der  '.
                                    'das Geschenk gebrauchen und wertschätzen kann.',

    'formhelp_searchterm'       => 'Geben Sie einen Suchbegriff',
    'checklisting_local'        => 'Nicht alle Artikel sind für die Sammlung zur Verfügung! '.
                                    'Immer die vollständige Liste überprüfen!',

    'desc_short_unwanted_title' => 'Ungewollte Geschenke',
    'desc_short_unwanted1'      => 'Diese Suchfunktion findet Artikel, die der Verkäufer '.
                                    'einfach nur loswerden und zu Geld machen will, z.B. alte Geschenke.',
    'desc_short_unwanted2'      => 'Vielleicht kennt der Verkäufer den wahren Wert des Gegenstandes nicht, oder '.
                                    'es ist ihm einfach egal. Er verdient dieses Geschenk nicht: Sie schon!'.
                                    'Die Unachtsamkeit des Verkäufers heißt für pfiffige Verkäufer wie Sie, '.
                                    'Artikel die unter Wert einfach hergegeben werden.',
    'desc_tiny_unwanted'        => 'Neue Artikel zu Second Hand Preisen!',

    'desc_long_furniture_title' => '<font color=\'red\'>NEU!!! </font>Local '.
                                    'Möbelsuche : Einrichtungsschnäppchen zum mitnehmen.',
    'desc_long_furniture1'      => '<b>Finden Sie Möbel zu Tiefstpreisen in ihrer Nähe!</b> ',
    'desc_long_furniture2'      => 'Benutzen Sie die Lokale Möbelsuche großartige Angebote für Tische, Stühle, '.
                                   'Sofas und andere große Einrichtungsgegenstände zu finden.',
    'desc_long_furniture3'      => 'Wir sind sowieso der Meinung, dass '.
                                    'der Verkauf von Möbeln bei eBay ein Fehler für die meisten Verkäufer ist. '.
                                    'Man kann halt keinen Sessel in eine Versandtasche stecken und losschicken.',
    'desc_long_furniture4'      => 'Aufgrund der riesigen Ausmaße, werden diese Artikel '.
                                    'meistens nur zum Abholen oder Lieferung über einen Kurierversand angeboten. '.
                                    'Wenn Sie zwei starke Arme und einen Bus haben können Sie davon profitieren , '.
                                    'und ein echtes Schnäppchen davontragen. Denken Sie dran,: '.
                                    'Unsere Suche ist speziell für Sie da!',

    'desc_short_furniture_title'=> 'Lokale Möbel Suche',
    'desc_short_furniture1'     => 'Finden Sie Möbel in Ihrer Nähe und zu Tiefstpreisen auf eBay!',
    'desc_tiny_furniture'       => 'Lokale Möbel zu Tiefstpreisen!',

    'desc_long_joblot_title'    => 'Lokale Restpostensuche : Günstig Artikel einkaufen, '.
                                    'dann gewinnbringend verkaufen.',
    'desc_long_joblot1'         => 'Unsere lokale Restpostensuche ist ideal um '.
                                    'günstige Schnäppchen in großen Mengen in Ihrer Nachbarschaft zu kaufen. '.
                                    'Kaufen Sie zum Spottpreis und verkaufen die Waren dann gewinnbringend '.
                                    'auf dem Flohmarkt oder sogar wieder bei eBay.',
    'desc_long_joblot2'         => 'BCglaubt, dass Hamsterkäufe die '.
                                    'besseren Schnäppchen bieten. Und ohne Versand - oder  '.
                                    'Zustellgebühren ist das Ganze umso besser für Sie.',
    'desc_long_joblot3'         => 'Verwenden Sie unsere Top Schnellsuche um genau '.
                                    'diese Restposten Angebote zu finden, nicht nur zu großartigen '.
                                    'Preisen sondern auch erhältlich ganz in Ihrer Nähe. Egal ob es sich um '.
                                    'Seife, Handwerkliche Gegenstände, Flohmarktartikel, Kleidung oder in Tat '.
                                    'jegliche andere Form von Artikeln handelt, eBay ist der ideale Platz um '.
                                    'solche Restposten spottbillig zu finden.',
    'desc_long_joblot4'         => 'Unser Team hier hat eine Suchfunktion '.
                                    'entwickelt die spezialisiert ist auf Verkäufe mit großer Warenmenge, '.
                                    'schöpfen Sie aus den Vollen in Ihrer direkten Umgebung.',

    'desc_short_joblot_title'   => 'Lokale Stellensuche',
    'desc_short_joblot1'        => 'Mengenangebote zu günstigen Preisen auf eBay! ',
    'desc_short_joblot2'        => 'Und wenn Sie keine Versand - oder Zustellgebühren '.
                                    'für Ihren Vorrat zahlen müssen, umso besser. ',
    'desc_tiny_joblot'          => 'Günstig kaufen, teuer verkaufen! ',

    'desc_long_baby_title'      => 'Baby Schnäppchensuche',
    'desc_long_baby1'           => '<b>Große Schnäppchen Für ihren kleinen Schatz!</b> ',
    'desc_long_baby2'           => 'Lokale Schnäppchen könnten genau vor Ihrer Haustür warten, '.
                                    'wenn also ein Baby in Planung oder schon da ist, '.
                                    'probieren Sie diese tolle kleine Suchfunktion um schnell '.
                                    'schnell eBay Angebote für günstige Baby Produkte in Ihrer '.
                                    'Nähe zu finden.',
    'desc_long_baby3'           => 'Ganz gleich ob Sie einen Kinderwagen, ein Gitterbett oder vielleicht '.
                                    'einen Babysitz furs Auto suchen, es gibt bestimmt ein passendes Angebot '.
                                    'auf eBay. Das allein ist schon praktisch, doch stellen Sie sich vor, Sie können '.
                                    'den Artikel einfach selbst abholen und sparen sich teure Versandgebühren.',

    'desc_short_baby_title'     => 'Baby Schnäppchensuche',
    'desc_short_baby1'          => 'Super Schnäppchen bei Kinderbetten und Kinderwägen auf eBay. ',
    'desc_tiny_baby'            => 'Große Schnäppchen zu kleinen Preisen',

    'desc_long_laptop_title'    => 'Lokale Laptop Suche',
    'desc_long_laptop1'         => 'Der sicherere Weg einen Laptop zu kaufen.',
    'desc_long_laptop2'         => 'Ihr günstigesGeschäft mit einem Laptop oder Netbook könnte zur teuren '.
                                    'Angelegenheit werden wenn jemand mit dem Paket nicht richtig umgeht.',
    'desc_long_laptop3'         => 'Lokal Schnäppchen zu kaufen, spart Zeit und Geld, bietet '.
                                    'Ihnen jedoch auch die Möglichkeit andere eBay Mitglieder persönlich '.
                                    'kennenzulernen. Es ist eine Vertrauenssache. Mit dem persönlichen '.
                                    'Kontakt haben beide Seiten Ihre Sicherheit: Sie können sich die Ware '.
                                    'vor dem Kauf genau ansehen und der Verkäufer sieht das Geld vor '.
                                    'der Warenübergabe. Der einzige Verlierer in diesem Szenario ist der '.
                                    'Zusteller, der nicht mehr gebraucht wird. '.
                                    'Und vielleicht erhalten Sie noch Gratis dazu ein paar tolle technische Tipps, '.
                                    'man kann ja nie wissen. Nutzen Sie jetzt unsere lokale Suche für Ihre nächste '.
                                    'technische, computerbezogene Anschaffung!',
    'desc_long_laptop4'         => 'Unsere clevere Suchfunktion "streift" quasi durch Ihre '.
                                    'virtuelle eBay Nachbarschaft und halt Ausschau nach allem '.
                                    'möglichen technischen Zubehör '.
                                    'Egal ob Sie ein Ipad, PC, Tastatur, Maus '.
                                    '(nicht die kleinen mit Fell), Dell, Acer, Mac '.
                                    'oder Sonstiges suchen, Sie können echte Schnäppchen finden, '.
                                    'Wahnsinns Preise in Fußnähe, bzw mit dem Fahrrad oder '.
                                    'Auto leicht zu erreichen. '.
                                    'Geben Sie einfach Ihre Postleitzahl ein wir zeigen Ihnen blitzschnell, '.
                                    'eine Liste von lokalen Computer Schnäppchen.',


    'desc_short_laptop_title'   => 'Lokale Laptop Suche',
    'desc_short_laptop1'        => 'Einige Gegenstände bieten beim Kauf oder Verkauf ein gewisses Risiko '.
                                    'gerade auf eBay, unter anderem Laptops und Netbooks ',
    'desc_short_laptop2'        => 'Lokal kaufen und den Verkäufer treffen. Unser '.
                                    'Lokale Laptop Suche Tool hilft ihnen ',
    'desc_tiny_laptop'          => 'Der sichere Weg einen Laptop zu kaufen',

    # Carparts
    'desc_long_carparts_title'  => 'Auto-Ersatzteile Suche lokal',

    'desc_long_carparts1'      => 'Unsere lokale Suche Car Parts helfen Ihnen, Car Parts '.
                                    'zu finden zu absolut günstigen Preisen, oft gleich um die Ecke!',
    'desc_long_carparts2'      => 'Oft, wenn Sie bereit, ein wenig weiter aus dem Weg zu gehen'.
                                    ' Können Sie greifen einige echte Schnäppchen, '.
                                    'wie zum Beispiel eine Reihe von Legierungen für'.
                                    'unter 50&euro;! Einfach Ihre Postleitzahl stellen und sehen.',
    'desc_long_carparts3'      => 'Geben Sie einfach ihre Postleitzahl ein und los gehts! '.
                                    'Geben Sie ein maximum für Preis und Entfernung an.
                                    Verfeinern Sie ihre Suche indem sie weitere Begriffe und Kategorien angeben. '.
                                    'Nachdem sie das Ersatzteil
                                    das Sie kaufen möchten gefunden haben,
                                    haben wir sogar ein Karte für Sie, '.
                                    'mit der Sie den Verkäufer finden können. Probieren Sie es aus!',

    'desc_short_carparts_title' => 'Auto-Ersatzteile Suche lokal',

    'desc_short_carparts1'      => 'Unsere lokale Suche Car Parts helfen Ihnen, Car Parts '.
                                    'zu finden zu absolut günstigen Preisen, oft gleich um die Ecke!',
    'desc_short_carparts2'      => 'Oft, wenn Sie bereit, ein wenig weiter aus dem Weg zu gehen'.
                                    ' Können Sie greifen einige echte Schnäppchen, '.
                                    'wie zum Beispiel eine Reihe von Legierungen für'.
                                    'unter 50&euro;! Einfach Ihre Postleitzahl stellen und sehen.',
    'desc_tiny_carparts' => 'Zubehör zu Tiefstpreisen!',

    # Cars
    'desc_long_cars_title'  => 'Lokale Auto Suche',
    'desc_long_cars1'       => '<em>Finden Sie Autos zu großartigen Preisen!</em>',
    'desc_long_cars2'       => 'Finden Sie ihr nächstes Auto, ganz nah, zu einem großartigen Preis!',

    'desc_short_cars_title' => 'Lokale Auto Suche',

    'desc_short_cars1'      => 'Finden Sie ihr nächstes Auto, ganz nah, zu einem großartigen Preis!',

    'desc_tiny_cars'        => 'Autos zu Tiefstpreisen!',

    'desc_long_amazon_title'   => 'Sonderfunktion: Suche auf Amazon',
 #  'desc_long_amazon1'        => 'BayCrazy ist die Webseite auf der '.
 #                                  'Schnäppchenjäger immer die besten Angebote finden können.
 #                                 Heutzutage muss man als Schnäppchenjäger umschauen und '.
 #                                  'Preise vergleichen um die besten Angebote zu finden.
 #                                  Das ist ein hervorrangender Grund um die '.
 #                                  'Amazon Suchfunktion von BayCrazy auszuprobieren.',
    'desc_long_amazon2'        => 'Als eines der größten Warenhäuser im Internet '.
                                    'und weltweit bietet Amazon.de fast alles was man sich vorstellen kann an.
                                    Ebenso vielfältig sind die Verkäufer die auf '.
                                    'dem Amazon Marktplatz ihre Waren anbieten.
                                    Vom Online Shop zu ihrem Nachbarn ist jeder vertreten.',
    'desc_long_amazon3'        => 'Wenn sie sich fragen ob der Auktionsgegenstand '.
                                    'den Sie gefunden haben wirklich ein Schnäppchen ist dann schauen
                                    Sie doch einfach mit unserer Suche auf Amazon nach. '.
                                    'Vielleicht finden Sie ihr Angebot dort für noch weniger Geld.',
    'desc_long_amazon4'        => 'Amazon verbindet sie mit über 2 Millionen Verkäufern '.
                                    'mit unzähligen Produkten und ihre hilft ihnen unter diesen
                                    die echten Schnäppchen zu finden. Geben Sie einfach '.
                                    'ein oder zwei Begriffe in unsere Suche ein und wir finden für
                                    Sie eine Auswahl an verfügbaren Angeboten.
                                    Wählen Sie dann einfach eines von diesen aus und '.
                                    'schon sind sie unterwegs zu Amazon wo sie ihr Schnäppchen erstehen können.',
 #   'desc_long_amazon5'        => 'BayCrazy modernisiert Online Shopping für Schnäppchenjäger.
 #                                   Mit unseren spezialisierten Suchfunktionen sind '.
 #                                   'Sie nur einen Klick entfernt von den besten Angeboten im Internet.
 #                                   Probieren Sie es aus! Suchen sie jetzt auf Amazon.de',

    # Buy It Now
    'desc_long_bin_title'  => 'Sofort-Kaufen Suche',
    'desc_long_bin1'       => '<em>Find listed items available for immediate sale.</em>',
    'desc_long_bin2'       => 'Try eBay for your next fixed price purchase. '.
                                'Although eBay is often remembered for it\'s '.
                                    'auction style listings, it is becoming'.
                                'ever more popular as a source of fixed price retail items.'.
                                'We can help you to find all sorts of bargains '.
                                    'available for you to buy immediately '.
                                'at a fixed price.',
    'desc_long_bin3'       => 'We apply special seller filters so that you are only '.
                                    'presented with listings where the seller '.
                                'has an established good reputation with eBay '.
                                    'and a no quibble returns policy.',
    'desc_long_bin4'       => 'Give our Buy It Now quick search page a try. Later you can refine your search.',

    'desc_short_bin_title' => 'Sofort-Kaufen',

    'desc_short_bin1'      => 'Find your next bargain, from a reputable seller, at a best fixed price!',

    'desc_tiny_bin'        => 'Bargains at good fixed prices!',

    'desc_short_amazon_title'   => 'Amazon Suche',
    'desc_short_amazon1'        => 'Bei Amazon nach Schnäppchen suchen, ',
    'desc_short_amazon2'        => 'einfach Schlagwort eingeben und suchen!',
    'desc_tiny_amazon'          => 'einfach Schlagwort eingeben und suchen!',

    # Buy it now

    /**
     * @todo: get native translation, was done using Google Translate.
     */

    'desc_long_buynow_title'    => 'Sofort-Kaufen',
    'desc_long_buynow1'        => 'jetzt kaufen, keine Wartezeiten für die Notierung bis Ende!',

    'desc_short_buynow1'        => 'jetzt kaufen, keine Wartezeiten für die Notierung bis Ende!',

    'desc_short_buynow_title'   => 'Sofort-Kaufen',
    'desc_tiny_buynow'          => 'jetzt kaufen, keine Wartezeiten für die Notierung bis Ende!',

    # Contact Page

    'contact_thanks'        => 'Vielen Dank für Ihre Anfrage an BayCrazy.de',
    'contact_acknowledge'   => 'Ihre Nachricht wurde nun an den Webmaster versendet!',
    'contact_nomessage'     => 'Sie müssen eine Nachricht eingeben. '.
                                'Versuchen Sie es noch einmal.',
    'contact_noemail'       => 'Sie haben keine Email Adresse angegeben: Wir '.
                                'werden Ihre Nachricht erhalten, können Ihnen aber nicht antworten.',
    'contact_close'         => 'Dieses Formular schließen',
    'contact_title'         => 'Wir hören immer gerne Ihre Rückmeldung, Kritik, Vorschläge , '.
                                'oder auch Lob.',
    'contact_faqtitle'      => 'Bitte lesen Sie zuerst die folgenden häufig gestellten Fragen '.
                                'und Antworten um Ihnen und uns etwas Zeit zu sparen!',
 #   'contact_qsell'         => 'Q: Wie kann ich auf BayCrazy.de verkaufen?',
 #   'contact_asell'         => 'A: BayCrazy.de ist eine Suchmaschine '.
                                'um Käufern Artikel aufzuzeigen, die unter Wert auf eBay angeboten werden '.
                                'Wir bieten keinen Verkauf an.',
#    'contact_qjoin'         => 'Q:Wie kann ich Mitglied bei BayCrazy.de werden ?',
#    'contact_ajoin'         => 'A: Das ist nicht notwendig! Sie können BayCrazy ganz ohne Anmeldung benutzen! '.
#                                 'Wichtig, um vollen nutzen aus BayCrazy zu ziehen sollten sie sich bei '.
#                                 '<a href=\'https://scgi.ebay.de/ws/eBayISAPI.dll?RegisterEnterInfo\''.
#                                    ' target=\'registerebay\'>'.
#                                 'ebay.de</a> ein Benutzerkonto erstellen. ',
    'contact_qbid'          => 'Q: Ich habe mehr geboten als ich eigentlich wollte, kann '.
                                'ich das Gebot zurücknehmen?',
    'contact_abid'          => 'A: Sie müssen den eBay Regeln folgen. Eine Anleitung finden '.
                                'Sie hier <a href=\'http://pages.ebay.de/help/buy/questions/'.
                                'retract-bid.html\' target=\'retract\'>HIER.</a> Handeln Sie sofort.',
    'contact_qcontact'      =>  'Falls Ihre Frage hier nicht beantwortet wird klicken sie hier um uns zu kontaktieren',
    'contact_text1'         => 'Alle Felder in diesem Formular sind freiwillig und '.
                                'jegliche angegebene Informationen werden streng unter Berücksichtung unserer '.
                                '<a href="/content/privacy" '.
                                'target="Privacy">Datenschutzerklärung</a> behandelt',
    'contact_text2'         => 'Wir werden Ihre Kontaktdaten nur verwenden, wenn Sie '.
                                '"Bitte kontaktieren Sie mich" auswählen',
    'contact_name'          => 'Ihr Name:',
    'contact_email'         => 'Ihre Email Adresse:',
    'contact_subject'       => 'Betreff:',
    'contact_p2'            => 'Anliegen:',
    'contact_contactme'     => 'Bitte kontaktieren Sie mich:',
    'contact_message'       => 'Ihre Nachricht:',
    'contact_send'          => 'Nachricht senden',

     #
    'desc_long_sell_title' => 'Selling on eBay',
 #   'desc_long_sell1'      => '<p>BayCrazy is an independent search site which operates as a benefit '.
 #                                   'to bargain hunters. We do not currently '.
 #                                   'offer any services to successful sellers. '.
 #                                 'All of the items which appear in our search results are '.
 #                                   'listed either on eBay, or on Amazon web sites. BayCrazy has no '.
 #                                   'part to play in the buying or selling process. All listing, buying '.
 #                                   'and selling is done on eBay or Amazon and is subject to their rules.</p>'.
 #                                 '<p>As a seller, you should really be disappointed if your items appear '.
 #                                   'on BayCrazy, because that would indicate that you have restricted '.
 #                                   'your market and will be destined to sell for a low price.'.
 #                                 'We are a site, mainly there to help buyers to find bargains.</p>'.
 #                                 '<p>We list some items which are currently on sale on eBay or Amazon, '.
 #                                   'and we try to select items which are '.
 #                                 'destined to reach an unusually low price.</p>'.
 #                                 '<p>If you want to sell on eBay, for a good price, '.
 #                                 'you must write your eBay listing well and our bargain hunters will '.
 #                                 'not see it on BayCrazy. If you write your eBay listing badly, '.
 #                                 'or restrict your market in some way, then your item might appear on '.
 #                                 'BayCrazy.com, but don\'t expect it to sell for a good price. '.
 #                                 'Items appearing on BayCrazy are there for the '.
 #                                 'bargain hunting buyers\' benefit.</p>',
    'desc_long_sell2'      => '<p>However, we realise that some of our users just wish to sell '.
                                  'locally and the selling price is not overly important.',

#    'desc_long_sell3'      => 'So. If you really do only want to sell as \'cash on collection only\' '.
#                                  'then what you need to do is list it on eBay. '.
#                                  'You should put the exact words \'cash on collection\' '.
#                                  'into the description part of your listing and choose the local '.
#                                  'collection shipping option. That might make your listing visible '.
#                                  'on BayCrazy to people in your local area.<br/>',#
#
#    'desc_long_sell4'      => 'BayCrazy.com does not charge anything for displaying items in '.
#                                  'its search results and it does not guarantee '.
#                                  'that any item will appear.<br/>',
#    'desc_long_sell5'      => 'When an item has received a few bids, or when BayCrazy stops '.
#                                  'considering it to be at a bargain price, then the item may '.
#                                  'stop showing in our search results. It will, of course '.
#                                  'still appear in regular ebay results.',

    'desc_long_sell6'      => 'If you are new to eBay, it might feel a bit daunting to set up '.
                                  'an account and list your items for sale. However, eBay have '.
                                  'now made it quite simple and they provide '.
                                  'helpful step by step guides.',
    'desc_long_sell7'      => 'First you need a regular eBay account. Skip this step if you '.
                                  'already have an eBay account. '.
                                  '<a href ="/outlink/redirect/page/signup/" target="signup">'.
                                  'Click here to sign up for an eBay account.</a>',
    'desc_long_sell8'      => 'If you wish to sell your first item, then you may need to upgrade '.
                                  'your eBay account to a \'seller account\'. '.
                                  '<a href ="/outlink/redirect/page/startselling/" target="startselling">'.
                                  'Click here to start selling.</a>',
    'desc_long_sell9'      => 'You might be asked to verify your identity by, perhaps, entering a phone number. '.
                                  'But it\'s all pretty straightforward. Just follow the prompts.'
                              .' If you are going to sell lots of items, '.
                                  'you might choose to set up a paypal account. But that is not immediately essential'
                              .' Then you need to decide whether you wish to sell at a fixed price, '.
                                  'or sell in an auction style listing. '.
                                  'There are a few decisions to make, and for now, '.
                                  'we suggest you try an auction style listing. But set your '.
                                  'starting price at the very minimum that you will accept'
                              .' You should prepare a photo of your item for sale, and you can usually '.
                                  'do that with a mobile phone or digital camera. For some items, '.
                                  'you can get a stock photo from the internet.',

    #Marc M alternative Local Index Page Experimental static page @ 2013-04-21
#    'desc_long_altlocal_title' => 'Lokale Schnäppchen : eBay lokale Schnäppchensuche ',
    
    
    'desc_long_altlocal1'      => '<div class="alt-landing">
<h1>Schnäppchensuche: Lokale Schnäppchen einfach finden</h1>
<img src="http://610b8cd8920a5625d88a-a84be1e8b1826794cca0c41cc6270567.r11.'.
                                  'cf3.rackcdn.com/release-2013-04-20-04/images/local.png" '.
                                  'alt="Lokale Schnäppchensuche" height="128" width="136">'.
                                  '<form action="/search.php" id="local_form" name="local_form" '.
                                  'method="get" class="search-form"><fieldset>
<input name="postcode" id="local_postcode" value="PLZ" class="text" type="text">
<input type="hidden" value=" " name="searchterm">
<input type="hidden" value="EBAY-DE" name="region">
<input id="catid" type="hidden" value="" name="catid">
<input type="hidden" value="1" name="pagenumber">
<input type="hidden" value="local" name="page">
<input onclick="recordOutboundLink(this, \'local\', \'search\');" '.
                                  'value="Schnellsuche" class="submitde" type="submit"></fieldset>
<script type="text/javascript">
var frmvalidator = new Validator("local_form");
frmvalidator.addValidation("local_postcode","numeric","Sie müssen eine vollständige deutsche Postleitzahl eingeben.");
frmvalidator.addValidation("local_postcode","req","Sie müssen eine vollständige deutsche Postleitzahl eingeben.");
frmvalidator.addValidation("local_postcode",'.
                                  '"maxlen=10","Sie müssen eine vollständige deutsche Postleitzahl eingeben.");
frmvalidator.addValidation("local_postcode","minlen=1",    
"Sie müssen eine vollständige deutsche Postleitzahl eingeben.");
</script></form>
<h2>Wie es funktioniert:</h2>
<div class="alt-landing-actions">
<div class="action" id="action-one"><span>1.</span>Postleitzahl im Suchfeld eingeben</div>
<div class="action" id="action-two"><span>2.</span>Suchen klicken</div>
<div class="action" id="action-three"><span>3.</span>Lokale Schnäppchen finden</div>
</div>
</div>',

   #Marc M alternative Local Furniture Page Experimental static page @ 2013-04-21
#    'desc_long_altfurniture_title' => 'Günstige Möbel in Ihrer Stadt',
    'desc_long_altfurniture1'      => '
<div class="alt-landing">
<h1>Schnäppchensuche: Lokale Möbel einfach finden</h1>
<img src="http://610b8cd8920a5625d88a-a84be1e8b1826794cca0c41cc6270567.r11.cf3.    
rackcdn.com/release-2013-03-10-06/images/furniture.png" width="180" height="90"     
alt="Lokale Möbel Suche" /><form action="/search.php" id="furniture_form"     
name="furniture_form" method="get" class="search-form"><fieldset>
<input type="text" name="postcode" id="furniture_postcode" value="PLZ" class="text"/>
<input onclick="recordOutboundLink(this, \'misspelled\', \'search\');"     
value="Schnellsuche" class="submitde" type="submit"></fieldset>
<script type="text/javascript">
var frmvalidator = new Validator("local_form");
frmvalidator.addValidation("local_postcode","numeric",    
"Sie müssen eine vollständige deutsche Postleitzahl eingeben.");
frmvalidator.addValidation("local_postcode","req",    
"Sie müssen eine vollständige deutsche Postleitzahl eingeben.");
frmvalidator.addValidation("local_postcode","maxlen=10",    
"Sie müssen eine vollständige deutsche Postleitzahl eingeben.");
frmvalidator.addValidation("local_postcode","minlen=1",    
"Sie müssen eine vollständige deutsche Postleitzahl eingeben.");
</script>
<input type="hidden" value=" " name="searchterm">
<input type="hidden" value="EBAY-DE" name="region">
<input id="catid" type="hidden" value="" name="catid">
<input type="hidden" value="1" name="pagenumber">
<input type="hidden" value="furniture" name="page">
</form>
<h2>Wie es funktioniert:</h2>
<div class="alt-landing-actions">
<div class="action" id="action-one"><span>1.</span>Postleitzahl im Suchfeld eingeben</div>
<div class="action" id="action-two"><span>2.</span>Suchen klicken</div>
<div class="action" id="action-three"><span>3.</span>Lokale Möbel finden</div>
</div>
</div>',

   # ebay403

    '403_title'         => 'Entschuldigung!!!',
    '403_text1'         => 'Es tut uns leid, wir können Ihre Anfrage im Moment '.
                            'nicht weiterleiten.',
    '403_text2'         => 'Ihr Browser teilt uns mit, dass Sie direkt von eBay '.
                            'zu uns kommen.',
    '403_text3'         => 'eBay hat, was diese Abfolge betrifft strenge Regeln '.
                            'und wir haben keine Erlaubnis die von Ihnen aufgerufene Seite '.
                            'im Moment darzustellen.',
    '403_text4'         => 'Um unsere kommerzielle Beziehung mit eBay zu waren, '.
                            'müssen wir Ihnen leider diese lästige Fehlermeldung anzeigen. ',
    '403_text5'         => 'Entschuldigung, es ist nicht wirklich ein Fehler, sondern von unserer Seite '.
                            'her der Versuch, unsere Seite innerhalb der eBay Reglen zu erhalten. ',
    '403_text6'         => 'Wir entschuldigen uns und versuchen weiterhin '.
                            'diesen Fehler zu beseitigen um die Anwendung unserer Seite für Sie '.
                            'so angenehm wie möglich zu machen.',
    '403_text7'         => 'Um diese Fehlermeldung in Zukunft zu umgehen, versuchen Sie bitte '.
                            'die "Seite vorwärts" und "Seite rückwärts" Pfeile in Ihrem Internet Browser Fenster '.
                            'zu benutzen, wenn Sie  '.
                            '<a href="www.baycrazy.de">www.baycrazy.de besuchen.',

    # manchester



    # search pages titles
    # pageTitle

    'misspelled_title'  => 'Schreibfehler Schnäppchen',
    'nightowl_title'    => 'späte Auktionsendzeiten',
    'unwanted_title'    => 'ungewollte Geschenke',
    'rightnow_title'    => 'Gebote die sofort enden',
    'joblot_title'      => 'Restposten Schnäppchen',
    'local_title'       => 'Lokale Schnäppchen',
    'carpart_title'     => 'Lokale Car Parts',
    'contact_title'     => 'Kontaktieren Sie uns',
    'faq_title'         => 'Häufig gestellte Fragen',
    'sell_title'        => 'Selling on eBay',
    # filtermenus

    'filter_try'        => 'Jetzt Ausprobieren!',
    'filter_pc'         => 'Geben Sie Ihre Postleitzahl ein',
    'filter_pc_us'      => 'Enter zip code',
    'filter_pc_uk'      => 'Enter postcode',
    'filter_pc_gb'      => 'Enter postcode',
    'filter_pc_au'      => 'Enter postal code',
    'filter_pc_ca'      => 'Enter postal code',
    'filter_pc_de'      => 'Geben Sie Ihre Postleitzahl ein',
    'filter_pc_de_num'  => 'Postleitzahlen dürfen nur Ziffern enthalten, keine Buchstaben '.
                            'oder Leerzeichen (Keine Zeichensetzung)',
    'filter_pc_us_num'  => 'You are using the USA version of the site '.
                            ' where Zip Code must be numeric characters only: '.
                            '(No spaces or commas)\nIf you are not in the USA, '.
                            'then click the correct country flag and try again.',
    'filter_pc_au_num'  => 'You are using the USA version of the site '.
                            ' where Zip Code must be numeric characters only: '.
                            '(No spaces or commas)\nIf you are not in the USA, '.
                            'then click the correct country flag and try again.',
    'filter_pc_uk_num'  => 'Postcodes may contain only letters and numbers, '.
                            'digits and spaces (No punctuation)',
    'filter_pc_ca_num'  => 'Postal codes may contain only letters, '.
                            'digits and spaces (No punctuation)',
    'filter_noinput'    => 'Bitten gebe Sie das korrekt geschriebene '.
                            'Suchwort ein',
    'filter_pc_max'     => 'Sie müssen eine vollständige deutsche Postleitzahl eingeben.',
    'filter_max'        => 'Die Maximallänge für das Suchwort beträgt '.
                            '30 Buchstaben',
    'filter_min'        => 'Die Mindestlänge für das Suchwort beträgt '.
                            '3 Buchstaben',
    'filter_invalid'    => 'Suchwörter dürfen nur Buchstaben, Zahlen und Leerzeichen '.
                            'enthalten (keine Zeichensetzung)',
    'filter_lc_header'  => 'Suchen Sie Artikel, die der Verkäufer nicht verschicken möchte, '.
                            'die aber ganz in Ihrer Nähe sind. <br />'.
                            'So erhalten Sie Hammerschnäppchen einfach zum abholen.',
    'filter_lc_title'   => 'Suche nach lokalen Schnäppchen',
    'filter_lc_text1'   => 'Suchen Sie Artikel, die der Verkäufer nicht verschicken möchte, '.
                            'die aber ganz in Ihrer Nähe sind. ',
    'filter_lc_text2'   => 'So erhalten Sie Hammerschnäppchen einfach zum '.
                           'abholen.',
    'filter_search'     => 'Jetzt suchen',
    'filter_refine'     => 'Suche verfeinern',
    'filter_local'      => 'Was heißt für Sie lokal?',

    'filter_buynow_title'    => 'Sofort-Kaufen',
    'filter_buynow_text1'        => 'jetzt kaufen, keine Wartezeiten für die Notierung bis Ende!',

    'another_category'  => 'Wählen Sie eine andere Kategorie',#todo

    'filter_uw_title'  => 'Suche nach ungewollten Geschenken ',

    'filter_uw_header' => 'Finden Sie Artikel, die der Verkäufer einfach nur  '.
                           'loswerden will.<br/>Vielleicht kennt der Verkäufer den echten Wert nicht '.
                           'oder es ist ihm auch egal.<br>'.
                            'Für den einen Müll, für Sie vielleicht ein Schatz.',

    'filter_uw_text1'  => 'Finden Sie Artikel, die der Verkäufer einfach nur  '.
                           'loswerden will.<br/>Vielleicht kennt der Verkäufer den echten Wert nicht '.
                           'oder es ist ihm auch egal.',
    'filter_uw_text2'  => 'Für den einen Müll, für Sie vielleicht ein Schatz.',
    'filter_ms_fb'     => '',

    'filter_ms_title'  => 'Suche nach Schreibfehlern',

    'filter_ms_header' => '<p>Manche achtlose Verkäufer schreiben wichtige Worte im Titel falsch.<br />'.
                          'Dadurch finden andere Bieter diese Angebote nur selten.</p>'.
                          '<h3>Weniger Bieter = Kleinere Preise</h3>'.
                          '<p>Bis zu 2 Suchwörter eingeben.<br>'.
                          'Ein Suchwort ist optimal<br>'.
                          'am Besten ein Markenname!<br>'.
                          '<span style=\'color:red\'><b>Sie MÜSSEN RICHTIG buchstabieren :o)</b></span><br>'.
                          'Geben Sie maximal 2 Suchwörter ein.<br/>Ein Markenname ist ideal!</p>'.
                          '<h3>Suche verfeinern</h3>',

    'filter_ms_text1'  => 'Manche achtlose Verkäufer schreiben wichtige Worte im Titel falsch.',
    'filter_ms_text2'  => 'Dadurch finden andere Bieter diese Angebote nur selten.',
    'filter_ms_text3'  => 'Weniger Bieter = Kleinere Preise',
    'filter_ms_text4'  => 'Bis zu 2 Suchwörter eingeben.<br/>'.
                            'Ein Suchwort ist optimal<br/>am Besten ein Markenname!<br/>',
    'filter_ms_text5'  => 'Sie MÜSSEN RICHTIG buchstabieren :o)',
    'filter_ms_text6'  => 'Geben Sie maximal 2 Suchwörter ein.<br/>Ein Markenname ist ideal!',

    'filter_am_title' => 'Amazon Suche',
    'filter_am_text1' => 'Amazon Suche für Schnäppchen, ',
    'filter_am_text2' => 'einfach Schlagwort eingeben und lossuchen!',

    'filter_no_title'  => 'Suche verrückte Angebotsendezeiten',
    'filter_no_text1'  => 'Manche achtlose Verkäufer lassen ihre Angebote mitten in der Nacht enden.',
    'filter_no_text2'  => 'So erhalten sie weniger Gebote in den letzten Minuten.',
    'filter_no_text3'  => 'Weniger Bieter = Kleinere Preise.',
    'filter_no_text4'  => 'Wann soll \'verrückte Zeiten\' beginnen?',
    'filter_no_text5'  => 'Wann soll \'verrückte Zeiten\' enden?',

    'filter_rn_title'  => 'Endet sofort : 0 Gebote',
    'filter_rn_text1'  => 'Sehen Sie nur, diese Angebote enden <b>\'jetzt.\'</b>',
    'filter_rn_text2'  => 'Diese Angebote enden in den nächsten Minuten: Niemand hat bisher geboten!',
    'filter_rn_text3'  => 'Schlagen Sie schnell noch bei diesen Schnäppchen zu!<br/>Jetzt oder nie!',
    'filter_rn_text4'  => 'Weniger Bieter = Kleinere Preise.',

    'filter_am_title' => 'Amazon Suche',
    'filter_am_text1' => 'Amazon Suche für Schnäppchen, ',
    'filter_am_text2' => 'einfach Schlagwort eingeben und lossuchen!',

    'filter_minprice'   => 'Mindestpreis?',
    'filter_maxprice'   => 'Wie viel möchten Sie ausgeben?',
    'filter_price2'     => 'Höchstpreis?',

    'filter_searchword'=> 'Suchbegriff eingeben (Optional)',

    'filter_fullsite'  => 'For more features, try the full site: ', #todo

    'footer_text1'     => ' ist eine unabhängige Suchmaschine und nicht Teil von eBay',
    'footer_text2'     => 'Datenschutzrichtlinie',
    'footer_text3'     => 'Über uns',
    'footer_text4'     => 'Handyseite',

    'cookies'           => 'Baycrazy.com verwendet Cookies. Einige können '.
                            'bereits eingestellt haben. Bitte klicken Sie auf '.
                            'die Schaltfläche, um unsere Cookies akzeptieren. '.
                            'Wenn Sie die Website weiter benutzen, nehmen wir '.
                            'an, dass du glücklich bist, um die Cookies '.
                            'anyaway akzeptieren.',

    'skip_nav'         => 'Zur Navigation springen (Enter drücken)',
    'skip_content'     => 'Zum Hauptmenü springen (Enter drücken)',

    'draw_local'       => 'Lokal Suchkriterium',
    'all_cats'         => 'Alle Kategorien',
    'all_subcats'      => 'Alle Unterkategorien',

    # tabluate

    'tab_price'        => 'Preis',
    'tab_shipping'     => 'Versand',
    'tab_end'          => 'Angebotsende',
    'tab_left'         => 'Restzeit',
    'tab_perpage'      => 'Artikel pro Seite',
    'tab_pic'          => 'Bildgröße',
    'tab_distance'     => 'Entfernung',

    'tab_error'        => 'UUPS! Ein Fehler ist aufgetreten und eBay '.
                           'hat keine Ergebnisse geliefert.<br/>',
    'tab_error1'       => 'Dies ist eine vorrübergehende Panne der eBay '.
                           'Suchfunktion. Wir haben eBay benachrichtigt und entschuldigen uns  '.
                           'für jegliche Unanehmlichkeiten.',
    'tab_error2'       => 'Bitte erneuern Sie diese Seite in einigen Minuten.',
    'tab_error115a'    => 'eBay zur Folge ist die eingegebene Postleitzahl '.
                           'ungültig für das Land in dem Sie suchen.',
    'tab_error115b'    => 'Wahrscheinlich haben Sie keine vollständige Postleitzahl  '.
                           'angegeben oder ein \'o\' oder \'O\' anstelle von einer '.
                           '\'0\' verwendet, dass ist der häufigste Grund für Fehler.',
    'tab_error115c'    => 'Oder Sie haben die richtige Postleitzahl angegeben aber '.
                           'wir suchen im falschen Land? Stellen Sie sicher, dass die richtige Flagge '.
                           'vergrößert wurde und rechts in der Reihe von Flaggen neben '.
                           'dieser Nachricht erscheint.',
    'tab_error115d'    => 'Sie haben diese Postleitzahl angegeben',

    'switch_country'    => 'Es sieht aus wie Sie Ihre Postleitzahl ist aus '.
                            'einem anderen Land. Wollen Sie zu wechseln: ',#todo
    'must_set'          => 'Sie müssen',#todo

    # api error

    'api_error_guess1' => 'eBay mag die eingegebene Postleitzahl wohl nicht (',
    'api_error_guess2' => '), also haben wir geraten und diese benutzt:',
    'api_error_guess3' => 'Um präzisere Ergebnisse zu erhalten, versuchen Sie eine andere Postleitzahl',
    'api_error_guess4' => 'Sie haben sicher die richtige Postleitzahl angegeben, bitte ',
    'api_error_guess5' => 'Status ',
    'api_error_guess6' => 'könnten sie die Postleitzahl bitte erneut eingeben? ',

    # Miscellaneous phrases
    'find_us_on_facebook' => 'finden Sie uns Facebook',
    'day'                 => 'T',
    'hour'                => 'Std',
    'min'                 => 'Min',
    'sec'                 => 'Sek',
    'filter_select_cat'   => 'In dieser Kategorie',
    'time_unit_plural'    => '',
    'bid_plural'          => 'e',
    'bid'                 => 'Gebot',
    'local_pickup'        => 'Persönlich Abholen',
    'filter_select_subcat'=> 'In dieser Unterkategorie',
    'add_a_comment'       => 'Kommentar hinzufügen',
    'api_error'           => 'UUPS! Es ist ein Fehler aufgetreten<p>Unsere Techniker wurden '.
                                 'informiert. Bitte starten sie ihre suche von der Startseite aus erneut. ',
    'default_postcode_warning' =>'Warnung: Sie haben nach der Standart Postleitzahl 80331 gesucht. '.
                            'Bitte achten sie darauf, Ihre eigene Postleitzahl einzugeben.',

    'map_to'             => 'Karte',

   'country_selection'   => 'Länderwahl',
    'settings'           => 'Einstellungen',

    'desc_long_switchcountry1'    => '<ul id="switchcountry">
                    <li class="uk"><a href="/local/index/region/UK/">UK</a></li>
                    <li class="us"><a href="/local/index/region/US/">US</a></li>
                    <li class="au"><a href="/local/index/region/AU/">Australia</a></li>
                    <li class="de"><a href="/local/index/region/DE/">Deutschland</a></li>
                    <li id="last" class="ca"><a href="/local/index/region/CA/">Canada</a></li>
              </ul>',

    'no_results'    => 'Sorry, es gibt keine Treffer angezeigt werden.', # @todo

    'no_results_long'   => '<h2>Sorry, does es scheinen es irgendwelche Ergebnisse zu zeigen.</h2>'
                            . '<p>Ein paar Dinge <strong>sollten Sie prüfen</strong>, um sicherzustellen, '
                            . 'dass es wirklich nichts zu sehen</p>'
                            . '<ul>'
                            . '<li>Ist die Postleitzahl richtig?</li>'
                            . '<li>Gibt es ein Stichwort eingegeben, die nicht sein sollte?</li>'
                            . '<li>Ist der Suchbereich zu klein?</li>'
                            . '<li>Ist der max Preis zu niedrig?</li>'
                            . '<li>Gibt es eine Kategorie-Set, das sollte vielleicht nicht da sein, '
                            . 'oder sollte etwas anderes sein?</li>'
                            . '</ul>',
    # ".$lang->getString('')."
);
