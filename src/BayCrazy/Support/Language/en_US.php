<?php

return array (

    'try_carparts'          => 'Try the \'Auto Parts\' search now!',
    'try_cars'              => 'Try the \'Auto\' search now!',

    'desc_long_local4'     => 'Our local search is unique in that we help you find the '.
                                'items listed as "pick up" or "collect in person only" as '.
                                'well as the items where a seller tries to offset price with '.
                                'high shipping costs. Use local search to find items listed '.
                                'in several categories. Click a link below for category '.
                                'details or just enter your zip code in our quick search to '.
                                'browse the complete range of available items.',
    'desc_long_local5'     => '<ul class=\'locallist\'>'.
                                '<li><a href="/carparts">local automotive</a></li>'.
                                '<li><a href="/furniture">furniture</a></li>'.
                                '<li><a href="/joblot">job lot bargains</a></li>'.
                                '<li><a href="/baby">Baby bargains</a></li>'.
                                '<li><a href="/laptop">local laptops</a></li></ul>',

    'desc_long_joblot4'        => 'It\'s easy to get started with local job lot searches here. '.
                                    'Just enter your zip code and click. Next, add '.
                                    'your job lot budget and the distance you\'ll travel to '.
                                    'pick up your job lot bargain. Further refine your search '.
                                    'by using keywords and/or categories and sub-categories. '.
                                    'Try it now!',

    'desc_long_baby4'           => 'We make things easy from start to finish. Simply begin '.
                                    'by adding your zip code to our form. Next add the price '.
                                    'you expect to pay and the distance you can travel to pick '.
                                    'up your merchandise. Refine your search with categories '.
                                    'and/or keywords. Let us help you shop local and '.
                                    'find the best bargain baby essentials. Our Baby '.
                                    'Bargain Search is the perfect tool to help you do that.'.
                                    ' Try it now!',

    'desc_short_carparts_title' => 'Local Auto Parts Search',

    'desc_short_carparts1'      => 'Great parts, on eBay at rock bottom '.
                                    'prices, often just around the corner!',
    'desc_short_carparts2'      => 'Our local Auto Parts search will help you to find auto parts, '.
                                    'go a little further out of your way and'.
                                    ' you can grab some real bargains, such as a set of alloys for '.
                                    'under $50!',

    # filtermenus

    'filter_pc'         => 'Enter zip code',

    'filter_pc_max'     => 'You must enter a full US zip code.\n'.
                            'It must only contain up to 6 numbers',

    'filter_cars_text1'   => 'eBay is now a great place to buy an automobile '.
                            'and we can help you find bargains which are in your local area. ',
    # ".$lang->getString('')."
);
