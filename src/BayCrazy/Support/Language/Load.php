<?php
/**
 * Created by PhpStorm.
 * User: thomasredstone
 * Date: 20/09/15
 * Time: 01:42
 */

namespace BayCrazy\Support\Language;

class Load
{

    public function addLoaders($translator, $locales) {

        $translator->addLoader('php', new \Symfony\Component\Translation\Loader\PhpFileLoader());
        foreach($locales as $locale) {
            $translator->addResource('php', __DIR__."/{$locale}.php", $locale);
        }
        $translator->addResource('php', __DIR__.'debug.php', 'debug');
        return $translator;
    }
}